import json
import numpy as np
from collections import Counter
import operator
import pandas as pd
from sklearn.model_selection import train_test_split

from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import Trainset


review_data = pd.read_csv('review_dataframe.csv',header=0, index_col=0)
# review = pd.DataFrame(review_data,columns=['user_id','business_id','stars'])
# review = review.sample(frac=0.25)
# testlist = ['jlu4CztcSxrKx56ba1a5AQ','5JVY32_bmTBfIGpCCsnAfw','iV3QtUHRJWmrUDUo3QETgQ','62GNFh5FySkA3MbrQmnqvg']

testlist = ['jlu4CztcSxrKx56ba1a5AQ','PKEzKWv_FktMm2mGPjwd0Q','bLbSNkLggFnqwNNzzq-Ijw','iV3QtUHRJWmrUDUo3QETgQ','Lfv4hefW1VbvaC2gatTFWA']
group_test_data = pd.DataFrame({})
for i in range(len(testlist)):
    u1 = testlist[i]
    user_review_part = review_data[review_data['user_id'] == u1]
    print(len(user_review_part))
    testdata = user_review_part.sample(frac=0.5)
    group_test_data = group_test_data.append(testdata)
    review_data.drop(list(testdata.index))


review = pd.DataFrame(review_data,columns=['user_id','business_id','stars'])
algo = SVD(n_epochs=20,n_factors=80)
reader = Reader(rating_scale=(1, 5))
traindata = Dataset.load_from_df(review[['user_id','business_id','stars']], reader)

X_train = traindata.build_full_trainset()

algo.fit(X_train)
df = pd.DataFrame([])
for test in testlist:
    # testsetindices = list(group_test_data.loc[group_test_data['user_id'] == test].index)
    print(test)
    testdata = group_test_data.loc[group_test_data['user_id'] == test]
    predicted_data = []
    for index, p in testdata.iterrows():
        predict = algo.predict(str(test),str(p['business_id']), r_ui=p['stars'])
        predicted_data.append({'user':predict[0],'item':predict[1], 'r_ui':predict[2], 'r_pr':predict[3]})

    pred = pd.DataFrame(predicted_data)
    sorted_pred = pred.sort_values(by=['r_pr'],ascending=False)
    result = sorted_pred.iloc[:50]
    # with open('use-case/single-top10.csv','a') as f:
    #     f.write(result)
    df = pd.concat([df, result], axis=0)
    # print(df)
df.to_csv (r'use-case/single-top10.csv', index = True, header=True)




