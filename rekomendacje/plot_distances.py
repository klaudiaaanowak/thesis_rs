import json
import numpy as np
from collections import Counter
from funcs import load_json_multiple
import operator
import pandas as pd
import matplotlib.pyplot as plt


with open(r'wyniki/distances_all.json', "r") as read_file:
    data = json.load(read_file)
print(data)
u = data[0]
print(u['user'])
x = u['x']
y = u['y']
plt.scatter(x,y)
plt.show()