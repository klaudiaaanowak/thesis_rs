import json
import numpy as np
from collections import Counter
from funcs import load_json_multiple
import operator
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from surprise.model_selection import cross_validate
from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import Trainset
import time
import threading
import multiprocessing

single_review=  pd.read_csv(r'review_dataframe.csv',header=0, index_col=0)

with open('business_groups_dict.json', 'r') as f:
    business_groups = json.load(f)

with open('users_groups_dict.json', 'r') as f:
    users_groups = json.load(f)

review_data = pd.read_csv('group_review_dataframe.csv',header=0, index_col=0)
review = pd.DataFrame(review_data,columns=['user','business','star'])
print("SVD 20/80")
def cross_validate_iteration(review, users_groups, business_groups,k, partofreview):
    print("start iteration ",k)
    algo = SVD(n_epochs=20,n_factors=80)
    err = 0

    X_train, X_test = train_test_split(partofreview, test_size=0.05)
    print("Test len: ",len(X_test))
    reader = Reader(rating_scale=(1, 5))
    traindata = Dataset.load_from_df(X_train[['user', 'business', 'star']], reader)
    testdata = Dataset.load_from_df(X_test[['user', 'business', 'star']], reader)

    X_train = traindata.build_full_trainset()
    algo.fit(X_train)

    testset = testdata.build_full_trainset().build_testset()
    predictions = algo.test(testset)
    print(len(predictions))
    for i in range(len(predictions)):
        rating = predictions[i][3]
        u_group = users_groups[str(predictions[i][0])]
        b_group = business_groups[predictions[i][1]]
        r = single_review[single_review['user_id'].isin(u_group)]
        rr = r[r['business_id'].isin(b_group)]
        s = np.array(rr['stars'])
        err += (sum(np.abs(s - rating)))/len(s)

    print("stop iteration ",k)
    print("partial error: ",  err/len(predictions))
    error_to_file = err/len(predictions)
    file_ = open("errors_groups.dat",'a')
    file_.write(str(k)+"\t"+str(error_to_file)+"\n")
    f.close()
    return err/len(predictions)



# avg_err = 0
# for j in range (5):

#     avg_err += err/len(predictions)
#     print("partial error: ",avg_err)
# print("error for all: ",avg_err/5)

# try:
if __name__ == "__main__":
    avg_err = 0

    file_ = open("errors_groups.dat",'a')
    file_.write("20/80\n*******************\n")
    f.close()

    partofreview1 = review.sample(frac=0.25)
    partofreview2 = review.sample(frac=0.25)
    partofreview3 = review.sample(frac=0.25)
    partofreview4 = review.sample(frac=0.25)
    partofreview5 = review.sample(frac=0.25)
    print(list(partofreview1['user'])[0])
    print(list(partofreview2['user'])[0])
    print(list(partofreview3['user'])[0])
    print(list(partofreview4['user'])[0])
    print(list(partofreview5['user'])[0])

    m1 = multiprocessing.Process(target=cross_validate_iteration, args=(review, users_groups, business_groups,1,partofreview1))
    m2 = multiprocessing.Process(target=cross_validate_iteration, args=(review, users_groups, business_groups,2,partofreview2))
    m3 = multiprocessing.Process(target=cross_validate_iteration, args=(review, users_groups, business_groups,3,partofreview3))
    m4 = multiprocessing.Process(target=cross_validate_iteration, args=(review, users_groups, business_groups,4,partofreview4))
    m5 = multiprocessing.Process(target=cross_validate_iteration, args=(review, users_groups, business_groups,5,partofreview5))
    
    m1.start()
    m2.start()
    m3.start()
    m4.start()
    m5.start()

    m1.join()
    m2.join()
    m3.join()
	

	
    m4.join()
    m5.join()
# except:
#     print ("Error")