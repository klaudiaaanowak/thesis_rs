import numpy as np
import json
import pandas as pd
from funcs import haversine

with open("categories.json",'r') as file:
    categories = json.load(file)

categories_list = categories.keys()

business_data = pd.read_csv('business_dataframe_all.csv',header=0, index_col=0)
del business_data['stars']

review_data = pd.read_csv('review_dataframe.csv',header=0, index_col=0)
review = pd.DataFrame(review_data,columns=['user_id','business_id','stars'])
users_list = np.array(review['user_id'])

print(len(users_list))

users_list = list(dict.fromkeys(users_list))

some_users = np.random.choice(users_list, 1, replace=False)
print(some_users)
distances = []
for user in some_users:
    user = '2U5VJvQ17FtB0wAmpaMowA'
    d = {}
    d['user']= user
    user_data = review.loc[review['user_id'] == user]
    # print(len(user_data))
    if (len(user_data) < 20):
        continue
    result = pd.merge(user_data, business_data, on='business_id')
    if(len(result)<1):
        continue
    for cat in categories_list:
        dist_ratings = []

        local_labels = result[result[cat] == True]
        # print(cat)
        if(len(local_labels) >0 ):
            print(local_labels)
        if (len(local_labels) < 5):
            continue
        business_list =  list(local_labels['business_id']) 

        for i in range(len(business_list)):
            b1 = result.loc[result['business_id'] == business_list[i]]
            for j in range(i+1,len(business_list)):
                b2 = result.loc[result['business_id'] == business_list[j]]
                dist = haversine(b1['latitude'].values[0],b1['longitude'].values[0], b2['latitude'].values[0],b2['longitude'].values[0])
                if(dist <= 2):
                    ratings = abs(b1['stars'].values[0] - b2['stars'].values[0])
                    dist_ratings.append((np.round(dist,4),ratings))
        x = [i for i,j in dist_ratings]
        y = [j for i,j in dist_ratings]
        if(len(x)>0):
            d[cat+"_x"] = x
            d[cat+"_y"] = y
    print(d)
    distances.append(d)
    with open('wyniki/distances_categories_old.json', 'a') as f:
        json.dump(d, f)
with open('wyniki/distances_categories_all_old.json', 'w') as f:
    json.dump(distances, f)