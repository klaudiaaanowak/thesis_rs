import json
import numpy as np
from collections import Counter
from funcs import haversine
import operator
import pandas as pd
import matplotlib.pyplot as plt


business_data = pd.read_csv('business_dataframe_all.csv',header=0, index_col=0)
del business_data['stars']

review_data = pd.read_csv('review_dataframe.csv',header=0, index_col=0)
review = pd.DataFrame(review_data,columns=['user_id','business_id','stars'])
users_list = np.array(review['user_id'])
# some_users = np.random.choice(users_list, 10, replace=False)
# print(some_users)
print(len(users_list))

users_list = list(dict.fromkeys(users_list))

print(len(users_list))
exit()
distances = []
i = 0
for user in users_list:
    dist_ratings = []
    d = {}
    d['user']= user
    user_data = review.loc[review['user_id'] == user]
    # print(len(user_data))
    result = pd.merge(user_data, business_data, on='business_id')
    # print(len(result))
    if(len(result)<1):
        continue
    # print(result['class'])
    local_labels = result['class'].value_counts()
    # print(local_labels.head())
    groups = {k:v for k, v in local_labels.items() if v>1}
    business_classes = list(groups.keys())
    print(business_classes)
    for c in business_classes:
        business_list =  list(result.loc[result['class'] == c]['business_id'])
        if(len(business_list)<2):
            continue
        for i in range(len(business_list)):
            b1 = result.loc[result['business_id'] == business_list[i]]
            for j in range(i+1,len(business_list)):
                b2 = result.loc[result['business_id'] == business_list[j]]
                dist = haversine(b1['latitude'].values[0],b1['longitude'].values[0], b2['latitude'].values[0],b2['longitude'].values[0])
                if(dist <= 2):
                    ratings = abs(b1['stars'].values[0] - b2['stars'].values[0])
                    dist_ratings.append((np.round(dist,4),ratings))
            # print(dist_ratings)
    x = [i for i,j in dist_ratings]
    y = [j for i,j in dist_ratings]
    if(len(x)>0):
        d['x'] = x
        d['y'] = y
        # print(d)
        distances.append(d)
        with open('wyniki/distances.json', 'a') as f:
            json.dump(d, f)
    print(i)
    i+=1

with open('wyniki/distances_all.json', 'w') as f:
    json.dump(distances, f)
