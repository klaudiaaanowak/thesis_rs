import json
import numpy as np
from collections import Counter
import operator
import pandas as pd
from sklearn.model_selection import train_test_split

from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import Trainset

with open('business_groups_dict.json', 'r') as f:
    business_groups = json.load(f)

with open('users_groups_dict.json', 'r') as f:
    users_groups = json.load(f)
org_review = pd.read_csv('review_dataframe.csv',header=0, index_col=0)

review_data = pd.read_csv('group_review_dataframe.csv',header=0, index_col=0)
review_data_sample = review_data.sample(frac=0.2)

print("SIZE: ",len(review_data_sample))


i = 0
group_test_data = pd.DataFrame({})
test_indices = pd.DataFrame({})
for index in range(0,len(review_data),10):
    # if(i > 30):
    #     break
    # # print(review_data.loc[index])
    # i+=1
    print(index)

    review = review_data.loc[index]
    if (review['star'] > 0):
        # print(review['business'],", ",review['user'])
        u_group = users_groups[str(review['user'])]
        # u_group = u
        b_group = business_groups[review['business']]
        r = org_review[org_review['user_id'].isin(u_group)]
        rr = r[r['business_id'].isin(b_group)]
        star_org = sum(rr['stars'] ) 
        # print(star_org/len(rr))
        test_data = rr.sample(frac=0.6)
        if(len(test_data) < 2):
            continue
        test_data['u_group'] = str(review['user'])
        test_data['b_group'] = review['business']
        star_test = (star_org - sum(test_data['stars'] ) ) / (len(rr) - len(test_data))
        print(review['star'],", ",(star_org/len(rr)), ", ",star_test)
        group_test_data = group_test_data.append(test_data)
        test_indices = test_indices.append(review)
        # print(review_data.loc[index]['star'],", ",star_test)
        review_data.loc[index,'star'] = star_test


algo = SVD(n_epochs=20,n_factors=80)
reader = Reader(rating_scale=(1, 5))
traindata = Dataset.load_from_df(review_data[['user', 'business', 'star']], reader)

X_train = traindata.build_full_trainset()

testdata = Dataset.load_from_df(test_indices[['user','business','star']], reader)
X_test_set = testdata.build_full_trainset()
X_test = X_test_set.build_testset()
algo.fit(X_train)

predictions = algo.test(X_test)

users = list(group_test_data['user_id'])
print(len(users))
users = list(Counter(list(group_test_data['user_id'])))
# print(len(user_count))
# users = [k for k,v in user_count.items() if v >= 2]
print(len(users))
predicted_error = []
for i in range(len(users)):
    u1 = users[i]
    part = group_test_data[group_test_data['user_id'] == u1]
    u_g = part['u_group'][0]
    b_g = list(part['b_group'])
    part_error = 0
    for b in b_g:
        pred_rating = [k for k in predictions if (str(int(k[0])) == u_g and k[1] == b)][0][3]
        part_buss = part[part['b_group'] == b]
        ratings = np.array(part_buss['stars'])
        part_error += np.sqrt(np.mean((pred_rating-ratings)**2))
    predicted_error.append({"user": u1, "rmse": part_error/len(b_g)})

df = pd.DataFrame(predicted_error)


df.to_csv (r'use-case/rsme_error_group.csv', index = True, header=True)