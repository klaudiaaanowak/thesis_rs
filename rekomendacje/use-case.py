import json
import numpy as np
from collections import Counter
import operator
import pandas as pd
from sklearn.model_selection import train_test_split

from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import Trainset

with open('business_groups_dict.json', 'r') as f:
    business_groups = json.load(f)
with open('users_groups_dict.json', 'r') as f:
    users_groups = json.load(f)
all_b = list(business_groups.keys())
business_data = pd.read_csv('business_classes.csv',header=0, index_col=0)

org_review = pd.read_csv('review_dataframe.csv',header=0, index_col=0)

user_data = pd.read_csv('users_classes.csv',header=0,index_col=0)

review_data = pd.read_csv('group_review_dataframe.csv',header=0, index_col=0)
users_group_list = np.random.choice(np.array(review_data['user']), 3, replace=False)
testlist = np.random.choice(np.array(users_groups[str(users_group_list[0])]), 10, replace=False)
# print(testlist)
# testlist = ['jlu4CztcSxrKx56ba1a5AQ','PKEzKWv_FktMm2mGPjwd0Q','bLbSNkLggFnqwNNzzq-Ijw','iV3QtUHRJWmrUDUo3QETgQ','Lfv4hefW1VbvaC2gatTFWA',testlist[0]]
testlist = ['jlu4CztcSxrKx56ba1a5AQ','PKEzKWv_FktMm2mGPjwd0Q','bLbSNkLggFnqwNNzzq-Ijw','iV3QtUHRJWmrUDUo3QETgQ','Lfv4hefW1VbvaC2gatTFWA',testlist[0]]
group_test_data = {}
for i in range(len(testlist)):
    u1 = testlist[i]
    user_review_part = org_review[org_review['user_id'] == u1]
    print(len(user_review_part))
    testdata = user_review_part.sample(frac=0.75)
    print(testdata)
    u_group = int(user_data.loc[u1]['class'])
    u_lists = users_groups[str(u_group)]
    group_test_data[str(u_group)] = []
    for index, row in testdata.iterrows():
        try:
            b_group = business_data.loc[row['business_id']]['class']
            b_lists = business_groups[b_group]
            rating = row['stars']
            print(b_group," ", u_group, ": ",rating)
            r = org_review[org_review['user_id'].isin(u_lists)]
            rr = r[r['business_id'].isin(b_lists)]
            s = np.array(rr['stars'])
            print(sum(s)/len(s))
            print(review_data[(review_data['user'] == u_group) & (review_data['business'] == b_group)])
            i = review_data[(review_data['user'] == u_group) & (review_data['business'] == b_group)].index[0]
            avg = (sum(s)-rating)/len(s)
            print(avg)
            # avg = 0
            review_data.loc[i, 'star'] = avg
            group_test_data[str(u_group)].append({'business':b_group, 'star': avg})
            print(review_data[(review_data['user'] == u_group) & (review_data['business'] == b_group)])
        except:
            continue
    used_b = review_data[review_data['user'] == u_group]['business']
    non_value_groups = list(set(all_b) - set(used_b))
    # for g in non_value_groups:
    #     group_test_data[str(u_group)].append({'business':g, 'star': 0})

review = pd.DataFrame(review_data,columns=['user','business','star'])

test_group_list = []
for test in testlist:
    int(user_data.loc[test]['class'])
    test_group_list.append(int(user_data.loc[test]['class']))
    print(group_test_data[str(test_group_list[-1])])
print(test_group_list)
algo = SVD(n_epochs=20,n_factors=80)
reader = Reader(rating_scale=(1, 5))
traindata = Dataset.load_from_df(review[['user', 'business', 'star']], reader)

X_train = traindata.build_full_trainset()

algo.fit(X_train)

# for i in range(len(test_group_list)):
for i in range(len(test_group_list)):

    testdata = group_test_data[str(test_group_list[i])]
    print("test data len: ",len(testdata))
    if(len(testdata) == 0):
        continue
    predicted_data = []
    for p in testdata:
        predict = algo.predict(str(test_group_list[i]),str(p['business']), r_ui=p['star'])
        predicted_data.append({'user':predict[0],'item':predict[1], 'r_ui':predict[2], 'r_pr':predict[3]})

    pred = pd.DataFrame(predicted_data)
    sorted_pred = pred.sort_values(by=['r_pr'],ascending=False)
    print(sorted_pred.iloc[:100])
    top10 = sorted_pred[:100]
    user_review_part = org_review[org_review['user_id'] == testlist[i]]
    print(len(user_review_part))
    up_top10 = []
    for index, t in top10.iterrows():
        b_group = business_groups[t['item']]
        # print(b_group)
        topreview = user_review_part[user_review_part['business_id'].isin(b_group)]
        # print(topreview)
        print(len(topreview))
        if(len(topreview) > 0):
            org_ratings = np.array(topreview['stars'])
            t['hit_rate'] = sum(org_ratings>4)/len(org_ratings)
            t['user'] = testlist[i]
            # print(t)
        else:
            t['hit_rate'] = 0
        up_top10.append(t)
    print(pd.DataFrame(up_top10))
    (pd.DataFrame(up_top10)).to_csv('use-case-wyniki/group.csv',mode='a',header=False)


