import numpy as np
import pandas as pd
import json

review =  pd.read_csv(r'review_dataframe.csv',header=0, index_col=0)

average_star_all_review = np.average(review['stars'])  # 3,716

# with open('business_groups_dict.json', 'r') as f:
# 	business_groups = json.load(f)

business = pd.read_csv('business_classes.csv',header=0, index_col=0)

users = pd.read_csv('users_regression_classes.csv',header=0, index_col=0)
# users = pd.read_csv('users_classes.csv',header=0, index_col=0)

review_data = pd.read_csv('regression_group_review_dataframe.csv',header=0, index_col=0)
# review_data = pd.read_csv('group_review_dataframe.csv',header=0, index_col=0)

average_star = np.average(review_data['star'])
print(average_star)
i = 0
data = []
for i, row in enumerate(review.values):

	u = row[0]
	b = row[1]
	star = row[2]

	try:
		u_class = users.loc[u,'class']
		b_class = business.loc[b,'class']
		# print(u_class, ", ", b_class, " : ", star)
		group_star = review_data[(review_data['user']==u_class) & (review_data['business'] == b_class)]['star'].values[0]
		row[2] = np.round(star + (average_star - group_star),3)

	except:
		print("not found ", i)
	data.append(row)


rw = pd.DataFrame(data)
rw.columns = review.columns
# for i, row in enumerate(rw.values):
# 	if i<10:
# 		print(row)
# 	else:
# 		break

rw.to_csv (r'preprocessed_review.csv', index = True, header=True)