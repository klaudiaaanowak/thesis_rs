import json
import numpy as np
from collections import Counter
from funcs import load_json_multiple
import operator
import pandas as pd
from sklearn.metrics import mean_squared_error


# # wczytaj business

business = pd.read_csv('business_classes.csv',header=0, index_col=0)

# # classes_labels_list = list(business['class'])
# # classes_labels = list(dict(Counter(classes_labels_list)))

# # business_groups = {}
# # for c in classes_labels:
# #     business_groups[c] = list((business.loc[business['class'] == c]).index)
# # with open('business_groups_dict.json', 'w') as f:
# #     json.dump(business_groups,f)

with open('business_groups_dict.json', 'r') as f:
    business_groups = json.load(f)


# #wczytaj użytkowników 
users = pd.read_csv('users_classes.csv',header=0, index_col=0)

# # classes_labels_list = list(users['class'])
# # classes_labels = list(dict(Counter(classes_labels_list)))

# # users_groups = {}
# # for c in classes_labels:

# #     users_groups[str(c)] = list((users.loc[users['class'] == c]).index)
# # with open('users_groups_dict.json', 'w') as f:
# #     json.dump(users_groups,f)
with open('users_groups_dict.json', 'r') as f:
    users_groups = json.load(f)


# wczytaj 

# review =  pd.read_csv(r'review_dataframe.csv',header=0, index_col=0)
# index = list(review.index)
# group_review = {}
# # id = 1
# for user_group in users_groups:
#     sum = 0
#     for user in users_group:

review =  pd.read_csv(r'review_dataframe.csv',header=0, index_col=0)
# index = list(review.index)
group_review = {}
# print(review['user_id'])
# print(review.loc[review['user_id'] == 'JXfRZ2jlLfU1BSeG_rw6Jg'])

# list_of_user_groups_ratings = []
# for group in users_groups.keys():
#     # print(group)
#     suma = 0
#     row = {}
#     for u in users_groups[group]:
#         review_list = review.loc[review['user_id'] == u].values
#         # if(len(review_list)>0):
#         #     # print(review_list)
#         for r in review_list:
#             # print(r[1], " ", r[2])
#             if not r[1] in row:
#                  row[r[1]] = 0
#             row[r[1]] = r[2]
#     list_of_user_groups_ratings.append(row)

# data = dict(list_of_user_groups_ratings) 
i = 0
data = {}
for i, row in enumerate(review.values):
    # print(row)
    u = row[0]
    b = row[1]
    star = row[2]
    # print(u, ", ", b, " : ", star)
    try:
        u_class = str(users.loc[u,'class'])
        b_class = business.loc[b,'class']
        # print(u_class, ", ", b_class, " : ", star)
        if (u_class not in data):
            data[u_class] = {}
        if (b_class not in data[u_class]):
            data[u_class][b_class] = 0
        data[u_class][b_class]+=star
    except:
        print("not found")
    # if i>30:
    #     break
    # i+=1

for d in data:
    for l in data[d]:

        data[d][l] =  round(data[d][l]/(len(users_groups[d])*len(business_groups[l])),3)

# print(data)
with open('temp_ratings.json', 'w') as f:
    json.dump(data,f)

df = pd.read_json (r'temp_ratings.json')
df = df.T
df = df.fillna(0)
df.to_csv (r'group_review_matrix.csv', index = True, header=True)
