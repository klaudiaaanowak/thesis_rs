import json
import numpy as np
from collections import Counter
from funcs import load_json_multiple
import operator
import pandas as pd
from sklearn.metrics import mean_squared_error

data = []
with open('review.json', 'r', encoding='utf-8',
                 errors='ignore') as f:
   for parsed_json in load_json_multiple(f):
       data.append(parsed_json)

df = pd.DataFrame.from_dict(data, orient='columns')
df_to_cluster = pd.DataFrame(df,columns=['user_id','business_id','stars','useful','funny','cool','date'])
df_to_cluster = df_to_cluster.set_index(df['review_id'])
df_to_cluster.to_csv (r'review_dataframe.csv', index = True, header=True)

