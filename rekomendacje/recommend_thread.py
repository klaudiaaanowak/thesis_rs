import json
import numpy as np
from collections import Counter
from funcs import load_json_multiple
import operator
import pandas as pd
from sklearn.metrics import mean_squared_error
from surprise.model_selection import cross_validate
from surprise import SVD
from surprise import Dataset
from surprise import Reader
import time
import multiprocessing



review_data = pd.read_csv('review_dataframe.csv',header=0, index_col=0)
review = pd.DataFrame(review_data,columns=['user_id','business_id','stars'])

reader = Reader(rating_scale=(1, 5))
data = Dataset.load_from_df(review[['user_id', 'business_id', 'stars']], reader)

def cross_validate_iteration(n_factors, data):
    print("Start iteration for factors: ",n_factors)
    algorithm = SVD(n_epochs=20,n_factors=n_factors)
    results = cross_validate(algorithm, data, measures=['RMSE','MAE'], cv=10, verbose=False)

    tmp = pd.DataFrame.from_dict(results).mean(axis=0)
    print(tmp)
    print("Stop iteration for factors: ",n_factors)
    file_ = open("errors_single.dat",'a')
    file_.write(str(tmp)+"\n")
    f.close()

if __name__ == "__main__":
    avg_err = 0
    file_ = open("errors_single.dat",'a')
    file_.write("SVD 20/80\n*****************\n")
    file_.close()
    cross_validate_iteration(80, data)
    #m1 = multiprocessing.Process(target=cross_validate_iteration, args=(80, data))
    # m2 = multiprocessing.Process(target=cross_validate_iteration, args=(80, data))
    # m3 = multiprocessing.Process(target=cross_validate_iteration, args=(80, data))
    # m4 = multiprocessing.Process(target=cross_validate_iteration, args=(80, data))
    #m1.start()
    # m2.start()
    # m3.start()
    # m4.start()

    #m1.join()
    # m2.join()
    # m3.join()   
    # m4.join()