import json
import numpy as np
from collections import Counter
from funcs import load_json_multiple
import operator
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from surprise.model_selection import cross_validate
from surprise import NMF
from surprise import Dataset
from surprise import Reader
from surprise import Trainset
import time

single_review=  pd.read_csv(r'review_dataframe.csv',header=0, index_col=0)

with open('business_groups_dict.json', 'r') as f:
    business_groups = json.load(f)

with open('users_groups_dict.json', 'r') as f:
    users_groups = json.load(f)


# with open('temp_ratings_nonzero.json', 'r') as f:
#     matrix = json.load(f)

# review = {}
# review['user'] = []
# review['business'] =[]
# review['star'] = []   
# for key in matrix.keys():
#     for label in matrix[key].keys():
#         review['user'].append(key)
#         review['business'].append(label)
#         review['star'].append(matrix[key][label])
#         # print(label)
# review_data = pd.DataFrame(review, columns=['user', 'business','star'])
# print(review_data.head())
# review_data.to_csv (r'group_review_dataframe.csv', index = True, header=True)

review_data = pd.read_csv('group_review_dataframe.csv',header=0, index_col=0)
review = pd.DataFrame(review_data,columns=['user','business','star'])

print("NMF 20/1000")

algo = NMF(n_epochs=20,n_factors=1000)
print(len(review))


# print(predictions)
# start = time.time()

avg_err = 0
for j in range (5):

    err = 0
    partofreview = review.sample(frac=0.25)
    X_train, X_test = train_test_split(partofreview, test_size=0.05)
    print("Test len: ",len(X_test))
    reader = Reader(rating_scale=(1, 5))
    traindata = Dataset.load_from_df(X_train[['user', 'business', 'star']], reader)
    testdata = Dataset.load_from_df(X_test[['user', 'business', 'star']], reader)

    X_train = traindata.build_full_trainset()
    algo.fit(X_train)

    testset = testdata.build_full_trainset().build_testset()
    predictions = algo.test(testset)
    print(len(predictions))
    for i in range(len(predictions)):
        # print(len(single_review))
        rating = predictions[i][3]
        u_group = users_groups[str(predictions[i][0])]
        b_group = business_groups[predictions[i][1]]
        r = single_review[single_review['user_id'].isin(u_group)]
        rr = single_review[single_review['business_id'].isin(b_group)]
        s = np.array(rr['stars'])
        err += (sum(np.abs(s - rating)))/len(s)
        # print(list(rr.index)) 
        # single_review = single_review.drop(list(rr.index))
    avg_err += err/len(predictions)
    print("partial error: ",avg_err)
print("error for all: ",avg_err/5)


print("NMF 20/80")

algo = NMF(n_epochs=20,n_factors=80)
print(len(review))


# print(predictions)
# start = time.time()

avg_err = 0
for j in range (5):

    err = 0
    partofreview = review.sample(frac=0.25)
    X_train, X_test = train_test_split(partofreview, test_size=0.05)
    print("Test len: ",len(X_test))
    reader = Reader(rating_scale=(1, 5))
    traindata = Dataset.load_from_df(X_train[['user', 'business', 'star']], reader)
    testdata = Dataset.load_from_df(X_test[['user', 'business', 'star']], reader)

    X_train = traindata.build_full_trainset()
    algo.fit(X_train)

    testset = testdata.build_full_trainset().build_testset()
    predictions = algo.test(testset)
    print(len(predictions))
    for i in range(len(predictions)):
        # print(len(single_review))
        rating = predictions[i][3]
        u_group = users_groups[str(predictions[i][0])]
        b_group = business_groups[predictions[i][1]]
        r = single_review[single_review['user_id'].isin(u_group)]
        rr = single_review[single_review['business_id'].isin(b_group)]
        s = np.array(rr['stars'])
        err += (sum(np.abs(s - rating)))/len(s)
        # print(list(rr.index)) 
        # single_review = single_review.drop(list(rr.index))
    avg_err += err/len(predictions)
    print("partial error: ",avg_err)
print("error for all: ",avg_err/5)

print("NMF 20/500")

algo = NMF(n_epochs=20,n_factors=500)
print(len(review))


# print(predictions)
# start = time.time()

avg_err = 0
for j in range (5):

    err = 0
    partofreview = review.sample(frac=0.25)
    X_train, X_test = train_test_split(partofreview, test_size=0.05)
    print("Test len: ",len(X_test))
    reader = Reader(rating_scale=(1, 5))
    traindata = Dataset.load_from_df(X_train[['user', 'business', 'star']], reader)
    testdata = Dataset.load_from_df(X_test[['user', 'business', 'star']], reader)

    X_train = traindata.build_full_trainset()
    algo.fit(X_train)

    testset = testdata.build_full_trainset().build_testset()
    predictions = algo.test(testset)
    print(len(predictions))
    for i in range(len(predictions)):
        # print(len(single_review))
        rating = predictions[i][3]
        u_group = users_groups[str(predictions[i][0])]
        b_group = business_groups[predictions[i][1]]
        r = single_review[single_review['user_id'].isin(u_group)]
        rr = single_review[single_review['business_id'].isin(b_group)]
        s = np.array(rr['stars'])
        err += (sum(np.abs(s - rating)))/len(s)
        # print(list(rr.index)) 
        # single_review = single_review.drop(list(rr.index))
    avg_err += err/len(predictions)
    print("partial error: ",avg_err)
print("error for all: ",avg_err/5)

print("NMF 20/200")

algo = NMF(n_epochs=20,n_factors=200)
print(len(review))


# print(predictions)
# start = time.time()

avg_err = 0
for j in range (5):

    err = 0
    partofreview = review.sample(frac=0.25)
    X_train, X_test = train_test_split(partofreview, test_size=0.05)
    print("Test len: ",len(X_test))
    reader = Reader(rating_scale=(1, 5))
    traindata = Dataset.load_from_df(X_train[['user', 'business', 'star']], reader)
    testdata = Dataset.load_from_df(X_test[['user', 'business', 'star']], reader)

    X_train = traindata.build_full_trainset()
    algo.fit(X_train)

    testset = testdata.build_full_trainset().build_testset()
    predictions = algo.test(testset)
    print(len(predictions))
    for i in range(len(predictions)):
        # print(len(single_review))
        rating = predictions[i][3]
        u_group = users_groups[str(predictions[i][0])]
        b_group = business_groups[predictions[i][1]]
        r = single_review[single_review['user_id'].isin(u_group)]
        rr = r[r['business_id'].isin(b_group)]
        s = np.array(rr['stars'])
        err += (sum(np.abs(s - rating)))/len(s)
        # print(list(rr.index)) 
        # single_review = single_review.drop(list(rr.index))
    avg_err += err/len(predictions)
    print("partial error: ",avg_err)
print("error for all: ",avg_err/5)
# reader = Reader(rating_scale=(1, 5))
# data = Dataset.load_from_df(review[['user', 'business', 'star']], reader)
# print(review.head())
# results = cross_validate(BaselineOnly(), data, measures=['RMSE','MAE'], cv=10, verbose=True)
# pd_res = pd.DataFrame(results)
# print(pd_res)

# pd_res.to_csv (r'group_review_results.csv', index = True, header=True)


# split = 10
# for k in list_k:
#     train_err_sum = 0
#     test_err_sum = 0
#     for i in range(split):
#         algo = BaselineOnly()
#         # k_means = MiniBatchKMeans(n_clusters=k, init_size=k)
#         X_train, X_test = train_test_split(data_normalized, test_size=0.4)
#         algo.fit(X_train)
#         predict = algo.predict(X_test)
#         print(predict)
#         centroids = k_means.cluster_centers_
#         centroids_lables = centroids[k_means.labels_]
#         err_train = mean_squared_error(X_train.to_numpy(), centroids_lables)
#         train_err_sum+=err_train  
#         predicted = k_means.predict(X_test)
#         centroids_lables_predicted = centroids[predicted]
#         test = X_test.to_numpy()
#         err_test = mean_squared_error(X_test.to_numpy(), centroids_lables_predicted)
#         test_err_sum +=err_test
#     train_err_sum = train_err_sum/split
#     test_err_sum = test_err_sum/split
#     sse_train.append(train_err_sum)
#     sse_test.append(test_err_sum/10)
#     err_avg = (train_err_sum+test_err_sum)/2
#     print("K: ",k, ", err_train: ",train_err_sum, ", err_test: ",test_err_sum,", avg err: ",err_avg)
#     sse.append(err_avg)