import json
import numpy as np
from collections import Counter
from funcs import load_json_multiple
import operator
import pandas as pd
from sklearn.metrics import mean_squared_error
from surprise.model_selection import cross_validate
from surprise import SVD, SVDpp, SlopeOne, NMF, NormalPredictor, KNNBaseline, KNNBasic, KNNWithMeans, KNNWithZScore, BaselineOnly, CoClustering
from surprise import Dataset
from surprise import Reader

review_data = pd.read_csv('group_review_dataframe.csv',header=0, index_col=0)
review = pd.DataFrame(review_data,columns=['user','business','star'])


reader = Reader(rating_scale=(1, 5))
data = Dataset.load_from_df(review[['user', 'business', 'star']], reader)

benchmark = []
# Iterate over all algorithms
for algorithm in [SVD(), SlopeOne(), NMF(), KNNBasic(), KNNWithMeans(), BaselineOnly(), CoClustering()]:
    # Perform cross validation
    results = cross_validate(algorithm, data, measures=['RMSE','MAE'], cv=10, verbose=False)
    
    # Get results & append algorithm name
    tmp = pd.DataFrame.from_dict(results).mean(axis=0)
    tmp = tmp.append(pd.Series([str(algorithm).split(' ')[0].split('.')[-1]], index=['Algorithm']))
    benchmark.append(tmp)
    print(tmp)
    
result = pd.DataFrame(benchmark).set_index('Algorithm').sort_values('test_rmse')    

result.to_csv (r'recommend_algorithm_comparison.csv', index = True, header=True)
