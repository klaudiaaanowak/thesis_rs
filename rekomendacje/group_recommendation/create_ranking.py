import json
from collections import Counter
import operator
from surprise import accuracy
import pandas as pd 
import numpy as np
import random
import quality_indicators
from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import Trainset
from sklearn.model_selection import train_test_split
review_data = pd.read_csv('review_normalized_clustering_data.csv',header=0, index_col=0)
business_data = pd.read_csv('business_clustering_data.csv', header=0, index_col=0)
restaurant_data = list(set(business_data[business_data['Restaurants']==True].index))
shopping_data = list(set(business_data[business_data['Shopping']==True].index))
services_data = list(set(business_data[business_data['Services']==True].index))




# review = review_data.sample(frac=0.05) # to change 
review = review_data
X_train, X_test = train_test_split(review, test_size=0.4) #to change
print("Test len: ",len(X_test))
cu = list(Counter(list(review['user_id'])))
cb = list(Counter(list(review['business_id'])))
print("all users: ",len (cu), ", all business: ", len(cb) )

reader = Reader(rating_scale=(1, 5))
traindata = Dataset.load_from_df(X_train[['user_id', 'business_id', 'stars']], reader)
testdata = Dataset.load_from_df(X_test[['user_id', 'business_id', 'stars']], reader)

X_train = traindata.build_full_trainset()
testset = testdata.build_full_trainset().build_testset()

algo = SVD(n_epochs=20,n_factors=80)
algo.fit(X_train)
predictions = algo.test(testset)

users_from_test = list(Counter(list(X_test['user_id'])))
print(len(users_from_test))
x_tr_test = X_train.build_testset()
uuu = Counter(list([x[0] for x in x_tr_test]))
users_from_train = [key  for (key, value) in uuu.items() if value > 20]
users = list(set(users_from_train).intersection(users_from_test))

# users = random.choices(users, k=int(0.4*len(users)))
print(len(users))

result = []

for i in range(len(users)):
    # if(i > 100):
    #     break
    u1 = users[i]
    part_user = X_test[X_test['user_id'] == u1]  
    bu = set(part_user['business_id'])
    part_pred = [k for k in predictions if k[0] == u1]
    test_predictions = np.array([x[1] for x in part_pred])  

    print(part_pred)  
    #restaurant
    b_data = list(set(test_predictions).intersection(restaurant_data))
    print("B_DATA: ",b_data)
    if(len(b_data)>0):
        business_to_test = quality_indicators.not_in_trainset(u1, review_data, restaurant_data)
        business_to_test = business_to_test.union(bu)
        raw_testset = Dataset.construct_testset(Dataset,[(u1, item, 0, 0) for item in business_to_test])
        dataset_test = Dataset.load_from_df(pd.DataFrame(raw_testset), reader)
        full_test = dataset_test.build_full_trainset().build_testset()
        user_full_predictions = algo.test(full_test)
        topN = quality_indicators.GetTopN(user_full_predictions)
        if(len(topN)>0):
            topN_items = np.array([x[0] for x in topN[u1]])
            restaurant_topN_count = sum(el in test_predictions for el in topN_items)/len(b_data)
            # print("Res: ",np.array([x[1] for x in topN[u1]]))
        else:
            restaurant_topN_count = 0
    else:
        restaurant_topN_count = None
    #shopping
    b_data = list(set(test_predictions).intersection(shopping_data))
    print("B_DATA: ",b_data)
    if(len(b_data)>0):
        business_to_test = quality_indicators.not_in_trainset(u1, review_data, shopping_data)
        business_to_test = business_to_test.union(business_to_test)
        raw_testset = Dataset.construct_testset(Dataset,[(u1, item, 0, 0) for item in business_to_test])
        dataset_test = Dataset.load_from_df(pd.DataFrame(raw_testset), reader)
        full_test = dataset_test.build_full_trainset().build_testset()
        user_full_predictions = algo.test(full_test)
        topN = quality_indicators.GetTopN(user_full_predictions)
        if(len(topN)>0):
            topN_items = np.array([x[0] for x in topN[u1]])
            shopping_topN_count = sum(el in test_predictions for el in topN_items)/len(b_data)
            # print("Shop: ",np.array([x[1] for x in topN[u1]]))
        else:
            shopping_topN_count = 0
    else:
        shopping_topN_count = None
    #services
    b_data = list(set(test_predictions).intersection(services_data))
    print("B_DATA: ",b_data)
    if(len(b_data)>0):
        business_to_test = quality_indicators.not_in_trainset(u1, review_data, services_data)
        business_to_test = business_to_test.union(business_to_test)
        raw_testset = Dataset.construct_testset(Dataset,[(u1, item, 0, 0) for item in business_to_test])
        dataset_test = Dataset.load_from_df(pd.DataFrame(raw_testset), reader)
        full_test = dataset_test.build_full_trainset().build_testset()
        user_full_predictions = algo.test(full_test)
        topN = quality_indicators.GetTopN(user_full_predictions)
        if(len(topN)>0):
            topN_items = np.array([x[0] for x in topN[u1]])
            services_topN_count = np.round(sum(el in test_predictions for el in topN_items)/len(b_data),3)
            # print("Service: ",np.array([x[1] for x in topN[u1]]))
        else:
            services_topN_count = 0
    else:
        services_topN_count = None

    rmse = accuracy.rmse(part_pred, verbose=True)

    result.append({'user_id':u1, 'restaurant_topN':restaurant_topN_count, 'shopping_topN':shopping_topN_count, 'services_topN':services_topN_count})
print("Full: ", accuracy.rmse(predictions))
df = pd.DataFrame(result)
df.to_csv(r"normalized_single_topN.csv", index=True, header=True)

