from surprise import accuracy
import pandas as pd 
import numpy as np
import quality_indicators

users = []
group_test_data = pd.DataFrame({})
prediction = pd.DataFrame({})

for i in range(len(users)):
# for i in range(10):
    u1 = users[i]
    part = group_test_data[group_test_data['user_id'] == u1]
    ranking_x = part.sort_values(by=['star'])
    predicted = prediction[prediction['user_id'] == u1]
    ranking_predicted = predicted.sort_values(by=['star'])
    mae = accuracy.mae(predicted, verbose=True)
    mse = accuracy.mse(predicted, verbose=True)
    rmse = accuracy.rmse(predicted, verbose=True)
    fcp = accuracy.fcp(predicted,verbose=True)
    mape  = quality_indicators.mape(predicted['org'], predicted['pred'])
    ndpm = quality_indicators.NDPM(ranking_x,ranking_predicted)

