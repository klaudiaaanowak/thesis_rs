import operator
from surprise import accuracy
import pandas as pd 
import numpy as np
import random
import quality_indicators
from collections import Counter


review_data = pd.read_csv('review_clustering_data.csv',header=0, index_col=0)

users = Counter(list(review_data['user_id']))
print("All users: ",len(users))
u50 =  {k: v for k, v in users.items() if v>50}
print("Users with more than 50 reviews: ",len(u50))
u20 =  {k: v for k, v in users.items() if v>20}
print("Users with more than 20 reviews: ",len(u20))
u10 =  {k: v for k, v in users.items() if v>10}
print("Users with more than 10 reviews: ",len(u10))

business = Counter(list(review_data['business_id']))
print("All businesses: ",len(business))
b50 =  {k: v for k, v in business.items() if v>50}
print("Businesses with more than 50 reviews: ",len(b50))
b20 =  {k: v for k, v in business.items() if v>20}
print("Businesses with more than 20 reviews: ",len(b20))
b10 =  {k: v for k, v in business.items() if v>10}
print("Businesses with more than 10 reviews: ",len(b10))

# All users:  260569
# Users with more than 50 reviews:  4073
# Users with more than 20 reviews:  15579
# Users with more than 10 reviews:  37726
# All businesses:  51485
# Businesses with more than 50 reviews:  4073
# Businesses with more than 20 reviews:  15579
# Businesses with more than 10 reviews:  37726