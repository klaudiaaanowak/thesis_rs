import json
from collections import Counter
from collections import defaultdict
import operator
from surprise import accuracy
import pandas as pd 
import numpy as np
import random
import quality_indicators
from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import Trainset
from sklearn.model_selection import train_test_split
review_data = pd.read_csv('review_clustering_data.csv',header=0, index_col=0)
business_data = pd.read_csv('business_clustering_data.csv', header=0)
business_list = business_data['business_id']

business_data_classes = pd.read_csv('new_without_geo_business_classes.csv',header=0)
# review = review_data.sample(frac=0.05) # to change 
review = review_data

cu = Counter(list(review['user_id']))
cb = list(Counter(list(review['business_id'])))
print("all users: ",len (cu), ", all business: ", len(cb) )

users = [key  for (key, value) in cu.items() if value > 10]
print(len(users))
user_list = random.choices(users, k=len(users))
set_size = int(np.floor(len(user_list)/10))

result = []

for k in range(10):
    test_users = user_list[k*set_size : k* set_size + set_size]
    review_users_all = review[review['user_id'].isin(test_users)] 
    test_data = pd.DataFrame({})
    for u in test_users:
        test_user_data = review_users_all[review_users_all['user_id']==u].sample(frac=0.5)
        test_data = test_data.append(test_user_data)
    # X_train, X_test = train_test_split(review, test_size=0.2) #to change
    train_org_data = review.drop(test_data.index)
    reader = Reader(rating_scale=(1, 5))
    traindata = Dataset.load_from_df(train_org_data[['user_id', 'business_id', 'stars']], reader)
    X_train = traindata.build_full_trainset()
    testdata = Dataset.load_from_df(test_data[['user_id', 'business_id', 'stars']], reader)
    X_test_set = testdata.build_full_trainset()
    X_test = X_test_set.build_testset()
    print(len(X_test))
    testset = testdata.build_full_trainset().build_testset()
    algo = SVD(n_epochs=20,n_factors=80)
    predictions = algo.fit(X_train).test(testset)
    users = list(Counter(list(test_data['user_id'])))
    print(len(users))


    for i in range(len(users)):
        u1 = users[i]
        print("index: ", i, " user: ",u1)
        part_pred = [k for k in predictions if k[0] == u1]
        # if (len(part_user)> 10):
        #     break
        rating_pred = np.array([x[3] for x in part_pred])
        sorted_rating_pred_indicies = np.argsort(rating_pred*(-1))
        rating_org = np.array([x[2] for x in part_pred])
        sorted_rating_org_indicies = np.argsort(rating_org*(-1))
        mae = accuracy.mae(part_pred, verbose=True)
        mse = accuracy.mse(part_pred, verbose=True)
        rmse = accuracy.rmse(part_pred, verbose=True)
        try:
            fcp = accuracy.fcp(part_pred,verbose=True)
        except:
            fcp = None
        mape  = quality_indicators.mape(rating_org, rating_pred)
        ndpm = quality_indicators.NDPM(sorted_rating_org_indicies,sorted_rating_pred_indicies)
        
        
        part_user = test_data[test_data['user_id'] == u1] 
        part_train_businesses = list(set(test_data[test_data['user_id'] == u1]['business_id']))
        bu = set(part_user['business_id'])
        train_bu_classes = list(set(business_data_classes[business_data_classes['business_id'].isin(list(part_train_businesses))]['class']))
        print(train_bu_classes)
        part_pred = [k for k in predictions if k[0] == u1]
        part_pred_up4 = [k for k in part_pred if k[3] >= 4]
        test_predictions = np.array([x[1] for x in part_pred_up4])  

        # print("PRED: ",part_pred_up4)  
        #restaurant
        b_data = list(set(test_predictions))
        # print("B_DATA: ",b_data)
        if(len(b_data)>0):
            business_to_test = quality_indicators.not_in_trainset(u1, review_data, business_list)
            business_to_test = business_to_test.union(bu)
            raw_testset = Dataset.construct_testset(Dataset,[(u1, item, 0, 0) for item in business_to_test])
            dataset_test = Dataset.load_from_df(pd.DataFrame(raw_testset), reader)
            full_test = dataset_test.build_full_trainset().build_testset()
            user_full_predictions = algo.test(full_test)
            topN = quality_indicators.GetTopN(user_full_predictions)
            # print("TOPN: ",topN)
            if(len(topN)>0):
                topN_items = np.array([x[0] for x in topN[u1]])
                topN_classes = list(set(business_data_classes[business_data_classes['business_id'].isin(list(topN_items))]['class']))
                print(topN_classes)
                topN_count = sum(el in test_predictions for el in topN_items)/len(b_data)
                print("TOPN COUNT: ", topN_count)
                # print([el in test_predictions for el in topN_items])
                # print([item for item in topN_classes if item not in train_bu_classes])
                novelty_count = len([item for item in topN_classes if item not in train_bu_classes])/len(topN_classes)
            else:
                topN_count = 0
                novelty_count = 0
        else:
            topN_count = None
            novelty_count = None
        
        result.append({'user_id': u1, 'mae':mae, 'mse':mse, 'rmse':rmse, 'fcp':fcp,'mape':mape,'ndpm':ndpm, 'topN':topN_count, 'novelty':novelty_count})

result_dict = pd.DataFrame(result)
result_dict.to_csv(r"quality_single_cross_validation.csv", index=True, header=True)




 