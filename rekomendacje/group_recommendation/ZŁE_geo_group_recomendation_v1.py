import json
import numpy as np
from collections import Counter
import operator
import pandas as pd
from sklearn.model_selection import train_test_split
import quality_indicators
import random

from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import Trainset

with open('new_business_groups_dict.json', 'r') as f:
    business_groups = json.load(f)

business = pd.read_csv('new_business_classes.csv',header=0, index_col=0)

with open('../users_regression_groups_dict.json', 'r') as f:
    users_groups = json.load(f)

users_classes = pd.read_csv('../users_regression_classes.csv',header=0, index_col=0)

org_review = pd.read_csv('review_clustering_data.csv',header=0, index_col=0)

review_data = pd.read_csv('new_regression_group_review_dataframe.csv',header=0, index_col=0)
# review_data_sample = review_data.sample(frac=0.2)
review_data = review_data.sample(frac=1).reset_index(drop=True)   
# print("SIZE: ",len(review_data_sample))


i = 0
group_test_data = pd.DataFrame({})
test_indices = pd.DataFrame({})
for index in range(0,len(review_data),10):
    # if(i > 500):
    #     break
    # print(review_data.loc[index])
    i+=1
    print(index)
    review = review_data.loc[index]
    if (review['star'] > 0):
        u_group = users_groups[str(review['user'])]
        b_group = business_groups[review['business']]
        r = org_review[org_review['user_id'].isin(u_group)]
        rr = r[r['business_id'].isin(b_group)]
        star_org = sum(rr['stars'] ) 
        test_data = rr.sample(frac=0.5)
        if(len(test_data) < 2):
            continue
        test_data['u_group'] = str(review['user'])
        test_data['b_group'] = review['business']
        star_test = (star_org - sum(test_data['stars'] ) ) / (len(rr) - len(test_data))
        print(review['star'],", ",(star_org/len(rr)), ", ",star_test)
        group_test_data = group_test_data.append(test_data)
        test_indices = test_indices.append(review)
        review_data.loc[index,'star'] = star_test

train_org_data = org_review.drop(group_test_data.index)

algo = SVD(n_epochs=20,n_factors=80)
reader = Reader(rating_scale=(1, 5))
traindata = Dataset.load_from_df(review_data[['user', 'business', 'star']], reader)

X_train = traindata.build_full_trainset()

testdata = Dataset.load_from_df(test_indices[['user','business','star']], reader)
X_test_set = testdata.build_full_trainset()
X_test = X_test_set.build_testset()
algo.fit(X_train)

predictions = algo.test(X_test)

users = list(group_test_data['user_id'])
print(len(users))
user_count = Counter(list(group_test_data['user_id']))

print(len(user_count))
users = [k for k,v in user_count.items() if v >= 2]
print(len(users))

predicted_error = []
for i in range(len(users)):
    u1 = users[i]
    part = group_test_data[group_test_data['user_id'] == u1]
    u_g = part['u_group'][0]
    u_g_user_list = users_classes[users_classes['class']==int(u_g)].index
    b_g = set(part['b_group'])
    part_error = 0
    part_abs_error = 0
    part_mape_error = 0
    part_user = [k for k in predictions if (str(int(k[0])) == u_g)]
    rating_pred = np.array([x[3] for x in part_user])
    sorted_rating_pred_indicies = np.argsort(rating_pred*(-1))
    rating_org = np.array([x[2] for x in part_user])
    sorted_rating_org_indicies = np.argsort(rating_org*(-1))
    prediction_business_list = np.array([x[1] for x in part_user])
    business_groups_full_test_set = random.sample(business_groups.keys(),100)
    business_groups_full_test_set.extend(b_g)
    fcp=None
    try:
        fcp = accuracy.fcp(part_user,verbose=True)
    except:
        fcp = None
    ndpm = quality_indicators.NDPM(sorted_rating_org_indicies,sorted_rating_pred_indicies)
    for b in b_g:
        pred_rating = [k for k in predictions if (str(int(k[0])) == u_g and k[1] == b)][0][3]
        part_buss = part[part['b_group'] == b]
        ratings = np.array(part_buss['stars'])
        part_error += np.mean((pred_rating-ratings)**2)
        part_abs_error += np.mean(abs(pred_rating-ratings))
        part_mape_error += np.mean(abs((ratings-pred_rating)/ratings))
    # print(predicted_error)

    raw_testset = Dataset.construct_testset(Dataset,[(u1, item, 0, 0) for item in business_groups_full_test_set])
    dataset_test = Dataset.load_from_df(pd.DataFrame(raw_testset), reader)
    full_test = dataset_test.build_full_trainset().build_testset()
    user_full_predictions = algo.test(full_test)
    best_items_groups = quality_indicators.GetTopN(user_full_predictions,n=30)[u1]
    # print("BEst groups: ", best_items_groups)
    war = np.any(np.in1d(best_items_groups, prediction_business_list))
    print(war)
    if(~war):
        print("should stop")
        predicted_error.append({"user": u1, "rmse": np.sqrt(part_error/len(b_g)), "mse": part_error/len(b_g), "mae": part_abs_error/len(b_g), "mape": part_mape_error/len(b_g), "fcp-group": fcp, "ndpm-group" : ndpm, "topN": 0})
        continue
    print("Stopped")
    user_org_data = train_org_data[train_org_data['user_id']==u1]
    business_reviewed = list(set(user_org_data['business_id']))
    ranking_list = []
    for item, score in best_items_groups:
        group_b_list = business[business['class']==item].index
        # print(len(group_b_list))
        for b in group_b_list:
            b_ratings = train_org_data[((train_org_data['business_id'] == b) & (train_org_data['user_id'].isin(u_g_user_list)))]['stars']
            # print(train_org_data[train_org_data['business_id'] == b])
            # print("user list: ",u_g_user_list)
            # print(train_org_data[train_org_data['user_id'].isin(u_g_user_list)])
            b_ratings = b_ratings[b_ratings >=4]
            print(len(b_ratings))
            if(len(b_ratings)>0):
                new_val = score * np.average(b_ratings)
                ranking_list.append({"business_id": b, "star": new_val})
            ranking_list.append({"business_id": b, "star": score})

    pd_raking = pd.DataFrame(ranking_list)
    pd_raking = pd_raking.sort_values(by=['star'],ascending=False)[:50]
    test_predictions_business = list(set(part['business_id']))
    # print(test_predictions_business)
    topN = list(pd_raking['business_id'])
    # print(topN)
    if(len(topN)>0):
        services_topN_count = np.round(sum(el in test_predictions_business for el in topN)/len(test_predictions_business),3)
        # print("Service: ",np.array([el in test_predictions_business for el in topN]))
    else:
        services_topN_count = 0 
    predicted_error.append({"user": u1, "rmse": np.sqrt(part_error/len(b_g)), "mse": part_error/len(b_g), "mae": part_abs_error/len(b_g), "mape": part_mape_error/len(b_g), "fcp-group": fcp, "ndpm-group" : ndpm, "topN": services_topN_count})

df = pd.DataFrame(predicted_error)


df.to_csv (r'quality_group_v1.csv', index = True, header=True)

