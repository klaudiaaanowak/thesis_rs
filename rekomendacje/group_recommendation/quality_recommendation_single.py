import json
from collections import Counter
from collections import defaultdict
import operator
from surprise import accuracy
import pandas as pd 
import numpy as np
import random
import quality_indicators
from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import Trainset
from sklearn.model_selection import train_test_split
review_data = pd.read_csv('review_clustering_data.csv',header=0, index_col=0)

# review = review_data.sample(frac=0.05) # to change 
review = review_data
X_train, X_test = train_test_split(review, test_size=0.2) #to change
print("Test len: ",len(X_test))
cu = list(Counter(list(review['user_id'])))
cb = list(Counter(list(review['business_id'])))
print("all users: ",len (cu), ", all business: ", len(cb) )

reader = Reader(rating_scale=(1, 5))
traindata = Dataset.load_from_df(X_train[['user_id', 'business_id', 'stars']], reader)
testdata = Dataset.load_from_df(X_test[['user_id', 'business_id', 'stars']], reader)

X_train = traindata.build_full_trainset()
testset = testdata.build_full_trainset().build_testset()

algo = SVD(n_epochs=20,n_factors=80)

predictions = algo.fit(X_train).test(testset)

users = list(Counter(list(X_test['user_id'])))
print(len(users))

users = random.choices(users, k=int(0.4*len(users)))
print(len(users))

result = []

for i in range(len(users)):
    u1 = users[i]
    print("index: ", i, " user: ",u1)
    part_user = [k for k in predictions if k[0] == u1]
    # if (len(part_user)> 10):
    #     break
    rating_pred = np.array([x[3] for x in part_user])
    sorted_rating_pred_indicies = np.argsort(rating_pred*(-1))
    rating_org = np.array([x[2] for x in part_user])
    sorted_rating_org_indicies = np.argsort(rating_org*(-1))
    mae = accuracy.mae(part_user, verbose=True)
    mse = accuracy.mse(part_user, verbose=True)
    rmse = accuracy.rmse(part_user, verbose=True)
    try:
        fcp = accuracy.fcp(part_user,verbose=True)
    except:
        fcp = None
    mape  = quality_indicators.mape(rating_org, rating_pred)
    ndpm = quality_indicators.NDPM(sorted_rating_org_indicies,sorted_rating_pred_indicies)
    result.append({'user_id': u1, 'mae':mae, 'mse':mse, 'rmse':rmse, 'fcp':fcp,'mape':mape,'ndpm':ndpm})

result_dict = pd.DataFrame(result)
result_dict.to_csv(r"quality_single.csv", index=True, header=True)




 