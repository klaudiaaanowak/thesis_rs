import json
import numpy as np
from collections import Counter
import operator
import pandas as pd

###############\
# Store data, which is used for clustering, geospatial information included in businesses and users data
#########
# business_groups_data = pd.read_csv('../business_classes.csv',header=0, index_col=0)
# business_groups_data.drop('class', axis=1, inplace=True)

# business_groups_data.to_csv(r'business_clustering_data.csv', index = True, header=True)

# user_groups_data = pd.read_csv("../users_regression_classes.csv", header=0,index_col=0)
# user_groups_data.drop(columns=['Restaurants','Shopping','Services','Beauty','Healthcare','Automotive','EducationNEntertainment','Home','Sport','class'])

# user_groups_data.to_csv(r'user_clustering_data.csv', index = True, header=True)

business_groups_data = pd.read_csv('business_clustering_data.csv',header=0)
user_groups_data = pd.read_csv("user_clustering_data.csv", header=0)

business_list = business_groups_data['business_id']
users_list = user_groups_data['user_id']
print("size b: ", len(business_list), " size u: ", len(users_list) )

review_data = pd.read_csv('../review_dataframe.csv',header=0, index_col=0)
print(len(review_data))

review_by_user = review_data[review_data['user_id'].isin(users_list)]
print(len(review_by_user))

review_clustering_data = review_by_user[review_by_user['business_id'].isin(business_list)]
print(len(review_clustering_data))


review_clustering_data.to_csv(r'review_clustering_data.csv', index = True, header=True)


