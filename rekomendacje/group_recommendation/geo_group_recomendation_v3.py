import json
import numpy as np
from collections import Counter
import operator
import pandas as pd
from sklearn.model_selection import train_test_split
import quality_indicators
import random
from sklearn.cluster import MiniBatchKMeans
from sklearn import decomposition
from sklearn.metrics import mean_squared_error

from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import Trainset

with open('new_business_groups_dict.json', 'r') as f:
    business_groups = json.load(f)

business = pd.read_csv('new_business_classes.csv',header=0)

# with open('../users_regression_groups_dict.json', 'r') as f:
#     users_groups = json.load(f)

# users_classes = pd.read_csv('../users_regression_classes.csv',header=0, index_col=0)
users_data = pd.read_csv('user_clustering_data.csv',header=0)

org_review = pd.read_csv('review_clustering_data.csv',header=0, index_col=0)

# review_data = pd.read_csv('new_regression_group_review_dataframe.csv',header=0, index_col=0)
# review_data = review_data.sample(frac=1).reset_index(drop=True)   

users_list = Counter(list(org_review['user_id']))
print(len(users_list))

users = [key  for (key, value) in users_list.items() if (value > 10 and value < 100)]
print(len(users))

# test_users = random.choices(users, k=int(0.2*len(users)))

test_users = random.choices(users, k=int(0.02*len(users)))

print(len(users_data))

user_data_normalized = users_data.copy()

user_data_normalized[['months_on_yelp','review_per_month','useful_per_month','funny_per_month','cool_per_month','reaction/comments','friend_count','fans','average_stars','activity_level']] = scaler.fit_transform(user_data_normalized[['months_on_yelp','review_per_month','useful_per_month','funny_per_month','cool_per_month','reaction/comments','friend_count','fans','average_stars','activity_level']].to_numpy())


new_user_data = user_data_normalized[~user_data_normalized['user_id'].isin(test_users)]
print(len(new_user_data))

#######################################################
# part user for test phase
new_user_data = new_user_data.sample(frac = 0.1)
print(len(new_user_data))

########################################################
new_org_review = org_review[~org_review['user_id'].isin(test_users)]
print(len(new_org_review))


## Clustering ##

k = 1000

k_means = MiniBatchKMeans(n_clusters=k, init_size=k)
k_means.fit(new_user_data)
centroids = k_means.cluster_centers_
centroids_lables = centroids[k_means.labels_]
error = mean_squared_error(new_user_data.to_numpy(), centroids_lables)
print("Error: ",error)

new_user_data['class'] = k_means.labels_

classes_labels = {}
for i in range(centroids.shape[0]):
    classes_labels[str(i)] = list(centroids[i])


users_groups = {}
for c in classes_labels:
    users_groups[str(c)] = list((new_user_data.loc[new_user_data['class'] == c]).index)



group_review = {}


i = 0
data = {}
for i, row in enumerate(new_org_review.values):
    u = row[0]
    b = row[1]
    star = row[2]
    # print(u, ", ", b, " : ", star)
    try:
        u_class = str(new_user_data.loc[u,'class'])
        b_class = business.loc[b,'class']
        # print(u_class, ", ", b_class, " : ", star)
        if (u_class not in data):
            data[u_class] = {}
        if (b_class not in data[u_class]):
            data[u_class][b_class] = {}
            data[u_class][b_class]['star'] = 0
            data[u_class][b_class]['counter'] = 0
        data[u_class][b_class]['star']+=star
        data[u_class][b_class]['counter']+=1
    except:
        print("not found")


ratings = {}
for d in data:
    ratings[d] = {}
    for l in data[d]:
        ratings[d][l] = 0
        if(data[d][l]['counter']>0):
            ratings[d][l] =  round(data[d][l]['star']/data[d][l]['counter'],3)
        else:
            ratings[d][l] = 0


new_review = {}
new_review['user'] = []
new_review['business'] =[]
new_review['star'] = []   
for key in ratings.keys():
    for label in ratings[key].keys():
        new_review['user'].append(key)
        new_review['business'].append(label)
        new_review['star'].append(ratings[key][label])
group_review_data = pd.DataFrame(new_review, columns=['user', 'business','star'])



# i = 0
# group_test_data = pd.DataFrame({})
# test_indices = pd.DataFrame({})
# for index in range(0,len(review_data),10):
#     # if(i > 500):
#     #     break
#     print(review_data.loc[index])
#     i+=1
#     print(index)
#     review = review_data.loc[index]
#     if (review['star'] > 0):
#         u_group = users_groups[str(review['user'])]
#         b_group = business_groups[review['business']]
#         r = org_review[org_review['user_id'].isin(u_group)]
#         rr = r[r['business_id'].isin(b_group)]
#         star_org = sum(rr['stars'] ) 
#         test_data = rr.sample(frac=0.5)
#         if(len(test_data) < 2):
#             continue
#         test_data['u_group'] = str(review['user'])
#         test_data['b_group'] = review['business']
#         star_test = (star_org - sum(test_data['stars'] ) ) / (len(rr) - len(test_data))
#         print(review['star'],", ",(star_org/len(rr)), ", ",star_test)
#         group_test_data = group_test_data.append(test_data)
#         test_indices = test_indices.append(review)
#         review_data.loc[index,'star'] = star_test


## Recomendations ##

train_org_data = new_org_review

algo = SVD(n_epochs=20,n_factors=80)
reader = Reader(rating_scale=(1, 5))
traindata = Dataset.load_from_df(group_review_data[['user', 'business', 'star']], reader)

X_train = traindata.build_full_trainset()

# testdata = Dataset.load_from_df(test_indices[['user','business','star']], reader)
# X_test_set = testdata.build_full_trainset()
# X_test = X_test_set.build_testset()
algo.fit(X_train)

# predictions = algo.test(X_test)





predicted_error = []
for i in range(len(test_users)):
    u1 = test_users[i]
    # part = group_test_data[group_test_data['user_id'] == u1]

    # znaleźć grupę najblizsżą uzytkownikowi 
    user_groups_labels = users_groups.copy()
    user_groups_labels.values = np.linalg.norm(user_groups_labels.values() - user_data_normalized[user_data_normalized['user_id'] == u1])
    user_grooups_labels_sorted = list(sorted(user_groups_labels.item(), key = lambda x : x[1]))
    u_g = user_grooups_labels_sorted[0]

    # wybrać 0.1 ocen z org_review dla u1
    part_user_review = org_review[org_review['user_id'] == u1].sample(frac=0.1)

    # znaleźć grupy biznesom, które ocenił
    reviewd_businesses = list(set(part_user_review['business_id']))
    reviewd_businesses_groups = {}
    for b in reviewd_businesses:
        reviewd_businesses_groups['b'] = business[business['business_id'].isin(reviewd_businesses)]['class']
    # reviewd_businesses_groups = list(set(business[business['business_id'].isin(reviewd_businesses)]['class']))
    # predykacja dla tych biznesów
    raw_testset = Dataset.construct_testset(Dataset,[(u_g, item, 0, 0) for item in list(set(reviewd_businesses_groups.values()))])
    dataset_test = Dataset.load_from_df(pd.DataFrame(raw_testset), reader)
    full_test = dataset_test.build_full_trainset().build_testset()
    user_full_predictions = algo.test(full_test)
    # porównanie oceny grupy, w której znajduje się biznes do orginalnych ocen 
    # policzenie "rmse": np.sqrt(part_error/len(b_g)), "mse": part_error/len(b_g), "mae": part_abs_error/len(b_g), "mape"

    part_error = 0
    part_abs_error = 0
    part_mape_error = 0
    for b in reviewd_businesses_groups.keys():
        pred_rating = [k for k in user_full_predictions if (k[1] == reviewd_businesses_groups[b])][0]
        rating = part_user_review[part_user_review['business_id'] == b]
        part_error += np.mean((pred_rating-ratings)**2)
        part_abs_error += np.mean(abs(pred_rating-ratings))
        part_mape_error += np.mean(abs((ratings-pred_rating)/ratings))
    predicted_error.append({"user": u1, "rmse": np.sqrt(part_error/len(b_g)), "mse": part_error/len(b_g), "mae": part_abs_error/len(b_g), "mape": part_mape_error/len(b_g)})

df = pd.DataFrame(predicted_error)


df.to_csv (r'quality_group_v3.csv', index = True, header=True)

