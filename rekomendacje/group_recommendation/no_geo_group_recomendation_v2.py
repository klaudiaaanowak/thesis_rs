import json
import numpy as np
from collections import Counter
import operator
import pandas as pd
from sklearn.model_selection import train_test_split
import quality_indicators
import random

from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import Trainset

with open('new_without_geo_business_groups_dict.json', 'r') as f:
    business_groups = json.load(f)

business = pd.read_csv('new_without_geo_business_classes.csv',header=0, index_col=0)

with open('../users_groups_dict.json', 'r') as f:
    users_groups = json.load(f)

users_classes = pd.read_csv('../users_classes.csv',header=0, index_col=0)

org_review = pd.read_csv('review_clustering_data.csv',header=0, index_col=0)

review_data = pd.read_csv('new_no_geo_group_review_dataframe.csv',header=0, index_col=0)
review_data = review_data.sample(frac=1).reset_index(drop=True)   


i = 0
group_test_data = pd.DataFrame({})
test_indices = pd.DataFrame({})
for index in range(0,len(review_data),10):
    # if(i > 500):
    #     break
    # print(review_data.loc[index])
    i+=1
    print(index)
    review = review_data.loc[index]
    if (review['star'] > 0):
        # print(review['business'],", ",review['user'])
        u_group = users_groups[str(review['user'])]
        b_group = business_groups[review['business']]
        r = org_review[org_review['user_id'].isin(u_group)]
        test_data = r[r['business_id'].isin(b_group)]
        if(len(test_data) < 2):
            continue
        test_data['u_group'] = str(review['user'])
        test_data['b_group'] = review['business']
        star_test = 0
        group_test_data = group_test_data.append(test_data)
        test_indices = test_indices.append(review)
        review_data.loc[index,'star'] = star_test

train_org_data = org_review.drop(group_test_data.index)

algo = SVD(n_epochs=20,n_factors=80)
reader = Reader(rating_scale=(1, 5))
traindata = Dataset.load_from_df(review_data[['user', 'business', 'star']], reader)

X_train = traindata.build_full_trainset()

testdata = Dataset.load_from_df(test_indices[['user','business','star']], reader)
X_test_set = testdata.build_full_trainset()
X_test = X_test_set.build_testset()
algo.fit(X_train)

predictions = algo.test(X_test)

users = list(group_test_data['user_id'])
print(len(users))
user_count = Counter(list(group_test_data['user_id']))

print(len(user_count))
users = [k for k,v in user_count.items() if v >= 10]
print(len(users))

predicted_error = []
for i in range(len(users)):
    u1 = users[i]
    part = group_test_data[group_test_data['user_id'] == u1]
    u_g = part['u_group'][0]
    u_g_user_list = users_classes[users_classes['class']==int(u_g)].index
    b_g = set(part['b_group'])
    part_error = 0
    part_abs_error = 0
    part_mape_error = 0
    part_user = [k for k in predictions if (str(int(k[0])) == u_g)]
    rating_pred = np.array([x[3] for x in part_user])
    sorted_rating_pred_indicies = np.argsort(rating_pred*(-1))
    rating_org = np.array([x[2] for x in part_user])
    sorted_rating_org_indicies = np.argsort(rating_org*(-1))
    fcp=None
    try:
        fcp = accuracy.fcp(part_user,verbose=True)
    except:
        fcp = None
    ndpm = quality_indicators.NDPM(sorted_rating_org_indicies,sorted_rating_pred_indicies)
    for b in b_g:
        pred_rating = [k for k in predictions if (str(int(k[0])) == u_g and k[1] == b)][0][3]
        part_buss = part[part['b_group'] == b]
        ratings = np.array(part_buss['stars'])
        part_error += np.mean((pred_rating-ratings)**2)
        part_abs_error += np.mean(abs(pred_rating-ratings))
        part_mape_error += np.mean(abs((ratings-pred_rating)/ratings))
    predicted_error.append({"user": u1, "rmse": np.sqrt(part_error/len(b_g)), "mse": part_error/len(b_g), "mae": part_abs_error/len(b_g), "mape": part_mape_error/len(b_g), "fcp-group": fcp, "ndpm-group" : ndpm})

df = pd.DataFrame(predicted_error)


df.to_csv (r'quality_no_geo_group_v2.csv', index = True, header=True)

