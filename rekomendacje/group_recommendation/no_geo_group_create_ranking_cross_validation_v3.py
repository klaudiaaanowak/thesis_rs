import json
import numpy as np
from collections import Counter
import operator
import pandas as pd
from sklearn.model_selection import train_test_split
import quality_indicators
import random
from sklearn.cluster import MiniBatchKMeans
from sklearn import decomposition
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import Normalizer
scaler = Normalizer()

from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import Trainset

with open('new_without_geo_business_groups_dict.json', 'r') as f:
    business_groups = json.load(f)

business = pd.read_csv('new_without_geo_business_classes.csv',header=0, index_col=0)
business_list = list(set(business['class']))

# with open('../users_regression_groups_dict.json', 'r') as f:
#     users_groups = json.load(f)

users_data = pd.read_csv('user_clustering_data.csv',header=0)
users_data = users_data.drop(columns=['class'])

org_review = pd.read_csv('review_clustering_data.csv',header=0, index_col=0)


users_list = Counter(list(org_review['user_id']))
print(len(users_list))

users = [key  for (key, value) in users_list.items() if (value > 10 and value < 100)]
print(len(users))
user_list = random.choices(users, k=len(users))
set_size = int(np.floor(len(user_list)/10))

user_data_normalized = users_data.copy()

user_data_normalized[['months_on_yelp','review_per_month','useful_per_month','funny_per_month','cool_per_month','reaction/comments','friend_count','fans','average_stars','activity_level']] = scaler.fit_transform(user_data_normalized[['months_on_yelp','review_per_month','useful_per_month','funny_per_month','cool_per_month','reaction/comments','friend_count','fans','average_stars','activity_level']].to_numpy())
user_data_normalized = user_data_normalized[['user_id','months_on_yelp','review_per_month','useful_per_month','funny_per_month','cool_per_month','reaction/comments','friend_count','fans','average_stars','activity_level']]

predicted_error = []


for k in range(10):
    test_users = user_list[k*set_size : k* set_size + set_size]

    new_user_data = user_data_normalized[(~user_data_normalized['user_id'].isin(test_users)) & (user_data_normalized['user_id'].isin(users_list))]
    new_user_data = new_user_data.set_index('user_id')
    print(len(new_user_data))

    ########################################################
    new_org_review = org_review[~org_review['user_id'].isin(test_users)]
    print(len(new_org_review))


    ## Clustering ##

    k = 1000

    k_means = MiniBatchKMeans(n_clusters=k, init_size=k)
    k_means.fit(new_user_data)
    centroids = k_means.cluster_centers_
    centroids_lables = centroids[k_means.labels_]
    error = mean_squared_error(new_user_data.to_numpy(), centroids_lables)
    print("Error: ",error)

    new_user_data['class'] = k_means.labels_

    classes_labels = {}
    for i in range(centroids.shape[0]):
        classes_labels[str(i)] = list(centroids[i])


    users_groups = {}
    for c in classes_labels:
        users_groups[str(c)] = list((new_user_data[new_user_data['class'] == int(c)]).index)



    group_review = {}


    j = 0
    data = {}
    for i, row in enumerate(new_org_review.values):
    #   if (j > 50000):
    #	    break
        j+=1
        u = row[0]
        b = row[1]
        star = row[2]
        #print(u, ", ", b, " : ", star)
        try:
            u_class = str(new_user_data.loc[u,'class'])
            b_class = business.loc[b,'class']
            # print(u_class, ", ", b_class, " : ", star)
            if (u_class not in data):
                data[u_class] = {}
            if (b_class not in data[u_class]):
                data[u_class][b_class] = {}
                data[u_class][b_class]['star'] = 0
                data[u_class][b_class]['counter'] = 0
            data[u_class][b_class]['star']+=star
            data[u_class][b_class]['counter']+=1
        except:
            print("not found")


    ratings = {}
    for d in data:
        ratings[d] = {}
        for l in data[d]:
            ratings[d][l] = 0
            if(data[d][l]['counter']>0):
                ratings[d][l] =  round(data[d][l]['star']/data[d][l]['counter'],3)
            else:
                ratings[d][l] = 0


    new_review = {}
    new_review['user'] = []
    new_review['business'] =[]
    new_review['star'] = []   
    for key in ratings.keys():
        for label in ratings[key].keys():
            new_review['user'].append(key)
            new_review['business'].append(label)
            new_review['star'].append(ratings[key][label])
    group_review_data = pd.DataFrame(new_review, columns=['user', 'business','star'])


    ## Recomendations ##

    train_org_data = new_org_review

    algo = SVD(n_epochs=20,n_factors=80)
    reader = Reader(rating_scale=(1, 5))
    traindata = Dataset.load_from_df(group_review_data[['user', 'business', 'star']], reader)

    X_train = traindata.build_full_trainset()

    # testdata = Dataset.load_from_df(test_indices[['user','business','star']], reader)
    # X_test_set = testdata.build_full_trainset()
    # X_test = X_test_set.build_testset()
    algo.fit(X_train)

    # predictions = algo.test(X_test)


    for i in range(len(test_users)):
        u1 = test_users[i]
        # part = group_test_data[group_test_data['user_id'] == u1]

        # znaleźć grupę najblizsżą uzytkownikowi 
        user_groups_labels = users_groups.copy()
        new_user_updated_data = user_data_normalized[user_data_normalized['user_id'] == u1]
        new_user_updated_data = new_user_updated_data.set_index('user_id')
        new_user_updated_data.loc[u1,['review_per_month', 'reaction/comments', 'average_stars', 'activity_level']] = 0
        diff = np.linalg.norm(np.array(list(classes_labels.values())) - np.array(new_user_updated_data), axis = 0)
        diff_dict = dict(zip(list(classes_labels.keys()), diff.tolist()))
        user_groups_labels_sorted = list(sorted(diff_dict.items(), key = lambda x : x[1]))
        u_g = user_groups_labels_sorted[0][0]

        # wybrać 0.1 ocen z org_review dla u1
        part_user_review = org_review[org_review['user_id'] == u1].sample(frac=0.1)

        # znaleźć grupy biznesom, które ocenił
        reviewed_businesses = list(set(part_user_review['business_id']))
        reviewed_businesses_groups = {}
        reviewed_businesses_groups= business[business.index.isin(reviewed_businesses)]['class']
        # reviewd_businesses_groups = list(set(business[business['business_id'].isin(reviewd_businesses)]['class']))
        # predykacja dla tych biznesów
        raw_testset = Dataset.construct_testset(Dataset,[(u_g, item, 0, 0) for item in list(set(reviewed_businesses_groups.values))])
        dataset_test = Dataset.load_from_df(pd.DataFrame(raw_testset), reader)
        full_test = dataset_test.build_full_trainset().build_testset()
        user_full_predictions = algo.test(full_test)
        # porównanie oceny grupy, w której znajduje się biznes do orginalnych ocen 
        # policzenie "rmse": np.sqrt(part_error/len(b_g)), "mse": part_error/len(b_g), "mae": part_abs_error/len(b_g), "mape"

        part_error = 0
        part_abs_error = 0
        part_mape_error = 0
        for b in reviewed_businesses_groups.keys():
            print(np.array(reviewed_businesses_groups[b]))
            if(np.array(reviewed_businesses_groups[b]).size > 1):
                continue
            print("ok")
            pred_rating = np.array([k[3] for k in user_full_predictions if (k[1] == reviewed_businesses_groups[b])])
            rating = np.array(part_user_review[part_user_review['business_id'] == b]['stars'])
            part_error += np.mean((pred_rating-rating)**2)
            part_abs_error += np.mean(abs(pred_rating-rating))
            part_mape_error += np.mean(abs(rating-pred_rating)/rating)
        
        ### RANKING ### 
        u_g_user_list = list(new_user_data[new_user_data['class']==int(u_g)].index)

        business_groups_test = set(reviewed_businesses_groups.keys())
    
        part_pred = [k for k in user_full_predictions if (str(int(k[0])) == u_g)]
        part_pred_up4 = [k for k in part_pred if k[3] >= 4]
        test_predictions = np.array([x[1] for x in part_pred_up4])  

        # print("PRED: ",part_pred_up4)  
        #restaurant
        b_data = list(set(test_predictions))
        # print("B_DATA: ",b_data)
        if(len(b_data)>0):
            business_to_test = quality_indicators.not_in_group_trainset(u1, group_review_data, business_list)
            business_to_test = business_to_test.union(business_groups_test)
            raw_testset = Dataset.construct_testset(Dataset,[(u1, item, 0, 0) for item in business_to_test])
            dataset_test = Dataset.load_from_df(pd.DataFrame(raw_testset), reader)
            full_test = dataset_test.build_full_trainset().build_testset()
            user_full_predictions = algo.test(full_test)
            topN = quality_indicators.GetTopN(user_full_predictions,n=20)
            # print("TOPN: ",topN)
            if(len(topN)>0):
                topN_items = np.array([x[0] for x in topN[u1]])
                # topN_classes = list(set(business_data_classes[business_data_classes['business_id'].isin(list(topN_items))]['class']))
                topN_count = sum(el in test_predictions for el in topN_items)/len(b_data)
                print("TOPN COUNT: ", topN_count)
                # novelty_count = sum(el in topN_items for el in train_bu_classes)/len(topN_items)
            else:
                topN_count = 0
        else:
            topN_count = None
        
        
        
        predicted_error.append({"user": u1, "rmse": np.sqrt(part_error/len(list(reviewed_businesses_groups.keys()))), "mse": part_error/len(list(reviewed_businesses_groups.keys())), "mae": part_abs_error/len(list(reviewed_businesses_groups.keys())), "mape": part_mape_error/len(list(reviewed_businesses_groups.keys())), 'topN':topN_count,})

        # print("Full: ", accuracy.rmse(predictions))
df = pd.DataFrame(predicted_error)
df.to_csv(r"group_no_geo_topN_novelty_v3_cross_validation.csv", index=True, header=True)
#df.to_csv(r"group_topN_novelty_v3_cross_validation.csv", index=True, header=True)

