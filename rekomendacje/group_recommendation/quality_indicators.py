import numpy as np
import random

from collections import defaultdict

def rmse(x, prediction):
    return np.sqrt(mse(x,prediction))

def mse(x,prediction):
    return np.sum((prediction-x)**2)/len(x)

def mae(x,prediction):
    return np.sum(abs(prediction-x))/len(x)

def mape(x,prediction):
    return np.sum(abs((x-prediction)/x))/len(x)

def NDPM(ranking_x, predicted_ranking): #Normalized Distance-based Performance Measure # 0 is perfect, the greater, ther worse
    c_plus = 0
    c_minus = 0
    c_u = 0
    c_s = 0
    if(len(ranking_x)<2):
        return None
    for i in range(len(ranking_x)):
        for j in range(len(ranking_x)):
            if(i!=j):
                c_plus+=np.sign(ranking_x[i] - ranking_x[j])*np.sign(predicted_ranking[i]-predicted_ranking[j])
                c_minus+=np.sign(ranking_x[i] - ranking_x[j])*np.sign(predicted_ranking[j]-predicted_ranking[i])
                c_u += (np.sign(ranking_x[i] - ranking_x[j]))**2
                c_s += (np.sign(predicted_ranking[i]-predicted_ranking[j]))**2
    c_u0 = c_u - (c_plus+c_minus)

    return (c_minus+0.5*c_u0)/c_u

def not_in_trainset(user_id, review_data, all_businesses):
    user_data = review_data[review_data['user_id']==user_id]
    # all_businesses = list(set(business_data.index))
    not_reviewed = list(set(all_businesses) - set(user_data['business_id']))
    not_reviewed_sample = random.choices(not_reviewed, k=int(0.2*len(not_reviewed)))

    return set(not_reviewed_sample)

def GetTopN(predictions, n=50, minimumRating=4.0):
    topN = defaultdict(list)
    for userID, itemID, actualRating, estimatedRating, _ in predictions:
        if (estimatedRating >= minimumRating):
            topN[userID].append((itemID, estimatedRating))

    for userID, ratings in topN.items():
        ratings.sort(key=lambda x: x[1], reverse=True)
        topN[userID] = ratings[:n]

    return topN

def getBestItemGroups(predictions, n=50, minimumRating=4.0):
    bests = []
    for userID, itemID, actualRating, estimatedRating, _ in predictions:
        if (estimatedRating >= minimumRating):
            bests.append((itemID, estimatedRating))
    return bests

