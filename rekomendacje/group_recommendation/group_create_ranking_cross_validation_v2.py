import json
from collections import Counter
import operator
from surprise import accuracy
import pandas as pd 
import numpy as np
import random
import quality_indicators
from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import Trainset
from sklearn.model_selection import train_test_split

#with open('new_without_geo_business_groups_dict.json', 'r') as f:
#    business_groups = json.load(f)

#business = pd.read_csv('new_without_geo_business_classes.csv',header=0, index_col=0)
#business_list = list(set(business['class']))

#with open('../users_groups_dict.json', 'r') as f:
#    users_groups = json.load(f)

#users_classes = pd.read_csv('../users_classes.csv',header=0, index_col=0)
with open('new_business_groups_dict.json', 'r') as f:
    business_groups = json.load(f)

business = pd.read_csv('new_business_classes.csv',header=0, index_col=0)
business_list = list(set(business['class']))

with open('users_regression_groups_dict.json', 'r') as f:
    users_groups = json.load(f)

users_classes = pd.read_csv('users_regression_classes.csv',header=0, index_col=0)

org_review = pd.read_csv('review_clustering_data.csv',header=0, index_col=0)



user_count = Counter(list(org_review['user_id']))
user_list = [k for k,v in user_count.items() if v >= 5]
user_list = random.choices(user_list, k=len(user_list))
set_size = int(np.floor(len(user_list)/10))

result = []

for k in range(10):
    review_data = pd.read_csv('new_regression_group_review_dataframe.csv',header=0, index_col=0)
    user_fold = user_list[k*set_size : k* set_size + set_size]
    i = 0
    group_test_data = pd.DataFrame({})
    test_indices = pd.DataFrame({})
    for index in range(0,len(review_data),5):
        # if(i > 500):
        #     break
        print(review_data.loc[index])
        i+=1
        print(index)
        review = review_data.loc[index]
        if (review['star'] > 0):
            u_group = users_groups[str(review['user'])]
            b_group = business_groups[review['business']]
            r = org_review[org_review['user_id'].isin(u_group)]
            rr = r[r['business_id'].isin(b_group)]
            test_data = rr[rr['user_id'].isin(user_fold)]
            test_data = test_data.sample(frac=0.5)
            if(len(test_data) < 2):
                continue
            test_data['u_group'] = str(review['user'])
            test_data['b_group'] = review['business']
            star_test = 0
            group_test_data = group_test_data.append(test_data)
            test_indices = test_indices.append(review)
            review_data.loc[index,'star'] = star_test

    train_org_data = org_review.drop(group_test_data.index)

    algo = SVD(n_epochs=20,n_factors=80)
    reader = Reader(rating_scale=(1, 5))
    traindata = Dataset.load_from_df(review_data[['user', 'business', 'star']], reader)

    X_train = traindata.build_full_trainset()

    testdata = Dataset.load_from_df(test_indices[['user','business','star']], reader)
    X_test_set = testdata.build_full_trainset()
    X_test = X_test_set.build_testset()
    algo.fit(X_train)

    predictions = algo.test(X_test)
    users = list(set(group_test_data['user_id']))


    for i in range(len(users)):
        # if(i > 20):
        #     break
        u1 = users[i]
        part_user_org = group_test_data[group_test_data['user_id'] == u1]
        if (len(part_user_org) == 0):
            continue
        u_g = part_user_org['u_group'][0]
        u_g_user_list = users_classes[users_classes['class']==int(u_g)].index
        business_groups_test = set(part_user_org['b_group'])
        part_error = 0
        part_abs_error = 0
        part_mape_error = 0
        part_user = [k for k in predictions if (str(int(k[0])) == u_g)]
        rating_pred = np.array([x[3] for x in part_user])
        sorted_rating_pred_indicies = np.argsort(rating_pred*(-1))
        rating_org = np.array([x[2] for x in part_user])
        sorted_rating_org_indicies = np.argsort(rating_org*(-1))
        fcp=None
        try:
            fcp = accuracy.fcp(part_user,verbose=True)
        except:
            fcp = None
        ndpm = quality_indicators.NDPM(sorted_rating_org_indicies,sorted_rating_pred_indicies)
        for b in business_groups_test:
            pred_rating = [k for k in predictions if (str(int(k[0])) == u_g and k[1] == b)][0][3]
            part_buss = part_user_org[part_user_org['b_group'] == b]
            ratings = np.array(part_buss['stars'])
            part_error += np.mean((pred_rating-ratings)**2)
            part_abs_error += np.mean(abs(pred_rating-ratings))
            part_mape_error += np.mean(abs((ratings-pred_rating)/ratings))


        part_train_user_org = train_org_data[train_org_data['user_id'] == u1]
        business_groups_test = set(part_user_org['b_group'])
        business_train = list(set(part_train_user_org['business_id']))
        train_bu_classes = list(set(business[business.index.isin(business_train)]['class']))
        # print(train_bu_classes)

        part_pred_up4 = [k for k in part_user if k[3] >= 4]
        test_predictions = np.array([x[1] for x in part_pred_up4])  

        b_data = list(set(test_predictions))
        # print("B_DATA: ",b_data)
        if(len(b_data)>0):
            business_to_test = quality_indicators.not_in_group_trainset(u1, review_data, business_list)
            business_to_test = business_to_test.union(business_groups_test)
            raw_testset = Dataset.construct_testset(Dataset,[(u1, item, 0, 0) for item in business_to_test])
            dataset_test = Dataset.load_from_df(pd.DataFrame(raw_testset), reader)
            full_test = dataset_test.build_full_trainset().build_testset()
            user_full_predictions = algo.test(full_test)
            topN = quality_indicators.GetTopN(user_full_predictions,n=20)
            # print("TOPN: ",topN)
            if(len(topN)>0):
                topN_items = np.array([x[0] for x in topN[u1]])
                # topN_classes = list(set(business_data_classes[business_data_classes['business_id'].isin(list(topN_items))]['class']))
                topN_count = sum(el in test_predictions for el in topN_items)/len(b_data)
                print("TOPN COUNT: ", topN_count)
                # novelty_count = sum(el in topN_items for el in train_bu_classes)/len(topN_items)
                novelty_count = len([item for item in topN_items if item not in train_bu_classes])/len(topN_items)

            else:
                topN_count = 0
                novelty_count = 0
        else:
            topN_count = None
            novelty_count = None


        rmse = accuracy.rmse(part_user, verbose=True)

        result.append({"user": u1, "rmse": np.sqrt(part_error/len(business_groups_test)), "mse": part_error/len(business_groups_test), "mae": part_abs_error/len(business_groups_test), "mape": part_mape_error/len(business_groups_test), "fcp-group": fcp, "ndpm-group" : ndpm,'topN':topN_count,'novelty':novelty_count})

        # print("Full: ", accuracy.rmse(predictions))
df = pd.DataFrame(result)
df.to_csv(r"group_topN_novelty_v2_cross_validation.csv", index=True, header=True)

