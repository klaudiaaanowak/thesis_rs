import json
import numpy as np
from collections import Counter
import operator
import pandas as pd
from sklearn.model_selection import train_test_split

from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import Trainset
from surprise import accuracy

review_data = pd.read_csv('review_dataframe.csv',header=0, index_col=0)
user_count = Counter(list(review_data['user_id']))
users = [k for k,v in user_count.items() if v >= 5]

group_test_data = pd.DataFrame({})
testindices = [] 
for i in range(len(users)):
# for i in range(30):
    u1 = users[i]
    user_review_part = review_data[review_data['user_id'] == u1]
    print(len(user_review_part))
    testdata = user_review_part.sample(frac=0.2)
    group_test_data = group_test_data.append(testdata)
    # print(len(group_test_data))
    # print(list(testdata.index))
    testindices.extend(list(testdata.index))
    # review_data = review_data.drop(list(testdata.index))
# print(testindices)
# print(len(review_data))

# review_data = review_data.drop(testindices)
print(len(testindices))
print(len(review_data))

review = pd.DataFrame(review_data,columns=['user_id','business_id','stars'])
algo = SVD(n_epochs=20,n_factors=80)
reader = Reader(rating_scale=(1, 5))
traindata = Dataset.load_from_df(review[['user_id','business_id','stars']], reader)
X_train = traindata.build_full_trainset()

testdata = Dataset.load_from_df(group_test_data[['user_id','business_id','stars']], reader)
X_test_set = testdata.build_full_trainset()
X_test = X_test_set.build_testset()
algo.fit(X_train)

predictions = algo.test(X_test)

full_error = accuracy.rmse(predictions, verbose=True)


predicted_error = []

for i in range(len(users)):
# for i in range(30):
    u1 = users[i]
    part_pred = [k for k in predictions if k[0] == u1]
    part_error = accuracy.rmse(part_pred, verbose=True)
    predicted_error.append({"user": u1, "rmse": part_error})

df = pd.DataFrame(predicted_error)


df.to_csv (r'use-case/rsme_error_single.csv', index = True, header=True)

