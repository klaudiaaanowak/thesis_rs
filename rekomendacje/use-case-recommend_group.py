import json
import numpy as np
from collections import Counter
import operator
import pandas as pd
from sklearn.model_selection import train_test_split

from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import Trainset

with open('business_groups_dict.json', 'r') as f:
    business_groups = json.load(f)

# with open('users_groups_dict.json', 'r') as f:
#     users_groups = json.load(f)
org_review = pd.read_csv('review_dataframe.csv',header=0, index_col=0)

user_data = pd.read_csv('users_classes.csv',header=0)

review_data = pd.read_csv('group_review_dataframe.csv',header=0, index_col=0)
review = pd.DataFrame(review_data,columns=['user','business','star'])
testlist = ['jlu4CztcSxrKx56ba1a5AQ','5JVY32_bmTBfIGpCCsnAfw','iV3QtUHRJWmrUDUo3QETgQ','62GNFh5FySkA3MbrQmnqvg']


# max items
#[('CxDOIDnH8gp9KXzpBHJYXw', 4129), ('bLbSNkLggFnqwNNzzq-Ijw', 2354), ('PKEzKWv_FktMm2mGPjwd0Q', 1822), ('ELcQDlf69kb-ihJfxZyL0A', 1764)]
test_group_list = []
for test in testlist:
    test_group_list.append(user_data.loc[user_data['user_id'] == test].values[0][-1])
print(test_group_list)
algo = SVD(n_epochs=20,n_factors=80)
reader = Reader(rating_scale=(1, 5))
traindata = Dataset.load_from_df(review[['user', 'business', 'star']], reader)

X_train = traindata.build_full_trainset()

algo.fit(X_train)

# for i in range(len(test_grou"151p_list)):
for i in range(len(test_group_list)):
    test = test_group_list[i]
    testsetindices = list(review.loc[review['user'] == test].index)
    # print(testsetindices)
    testdata = review.loc[testsetindices,:]


    predicted_data = []
    for index, p in testdata.iterrows():
        predict = algo.predict(str(test),str(p['business']), r_ui=p['star'])
        predicted_data.append({'user':predict[0],'item':predict[1], 'r_ui':predict[2], 'r_pr':predict[3]})
    pred = pd.DataFrame(predicted_data)
    sorted_pred = pred.sort_values(by=['r_pr'],ascending=False)
    print(sorted_pred.iloc[:100])
    top10 = sorted_pred[:100]
    user_review_part = org_review[org_review['user_id'] == testlist[i]]
    print(len(user_review_part))
    up_top10 = []
    for index, t in top10.iterrows():
        # print(t)
        b_group = business_groups[t['item']]
        # print(b_group)
        topreview = user_review_part[user_review_part['business_id'].isin(b_group)]
        # print(topreview)
        print(len(topreview))
        if(len(topreview) > 0):
            org_ratings = np.array(topreview['stars'])
            t['hit_rate'] = sum(org_ratings>4.5)/len(org_ratings)
            # t['user'] = testlist[i]
            print(t)
        else:
            t['hit_rate'] = 0
        up_top10.append(t)
    print(pd.DataFrame(up_top10))


# for u in u1:
#     user_part = org_review[org_review['user_id'] == u]
#     for b in b1:
#         b_g = business_groups[b]
#         topr = user_part[user_part['business_id'].isin(b_g)]
#         if(len(topr)>0):
#             print(u," , ", b)


# # # # Then sort the predictions for each user and retrieve the k highest ones.
# # # for uid, user_ratings in top_k.items():
# # #     user_ratings.sort(key=lambda x:x[1], reverse=True)
# # #     top_k[uid] = user_ratings[:k]
# # # toy_story_neighbors = algo.get_neighbors(toy_story_inner_id, 10)
# # # logging.debug('neighbors_ids=' + str(toy_story_neighbors))
# # # #The internal id of the model is converted to the actual movie id
# # # neighbors_raw_ids = [algo.trainset.to_raw_iid(inner_id) for inner_id in toy_story_neighbors]
# # # #Get a movie id list or a movie recommendation list
# # # neighbors_movies = [rid_to_name[raw_id] for raw_id in neighbors_raw_ids]
# # # print('The 10 nearest neighbors of Toy Story are:')
# # # for movie in neighbors_movies:
# # #     print(movie)

# # # testset = testdata.build_full_trainset().build_testset()
# # # predictions = algo.test(testset)
# # # print(len(predictions))
# # # for i in range(len(predictions)):
# # #     rating = predictions[i][3]
# # #     u_group = users_groups[str(predictions[i][0])]
# #     b_group = business_groups[predictions[i][1]]
# #     r = single_review[single_review['user_id'].isin(u_group)]
# #     rr = r[r['business_id'].isin(b_group)]
# #     s = np.array(rr['stars'])
# #     err += (sum(np.abs(s - rating)))/len(s)

# # print("stop iteration ",k)
# # print("partial error: ",  err/len(predictions))
# # error_to_file = err/len(predictions)
# # file_ = open("errors_groups.dat",'a')
# # file_.write(str(k)+"\t"+str(error_to_file)+"\n")
# # f.close()
# # return err/len(predictions)



