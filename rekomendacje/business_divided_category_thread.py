import numpy as np
import json
import pandas as pd
from funcs import haversine
import multiprocessing

with open("categories.json",'r') as file:
    categories = json.load(file)

categories_list = categories.keys()

business_data = pd.read_csv('business_dataframe_all.csv',header=0, index_col=0)
del business_data['stars']

review_data = pd.read_csv('review_dataframe.csv',header=0, index_col=0)
review = pd.DataFrame(review_data,columns=['user_id','business_id','stars'])
users_list = np.array(review['user_id'])

print(len(users_list))

users_list = list(dict.fromkeys(users_list))
print(len(users_list))

# some_users = np.random.choice(users_list, 1, replace=False)
def user_ratings_distance(start, end, filename):
    print("start iteration ",filename)
    distances = []
    some_users = users_list[start:end]
    # some_users = np.random.choice(users_list, 10, replace=False)
    for user in some_users:
        d = {}
        d['user']= user
        user_data = review.loc[review['user_id'] == user]
        print(len(user_data))
        if (len(user_data) < 50):
            continue
        result = pd.merge(user_data, business_data, on='business_id')
        if(len(result)<1):
            continue
        for cat in categories_list:
            dist_ratings = []
            local_labels = result[result[cat] == True]
            print(cat)
            if(len(local_labels) >0 ):
                print(local_labels)
            if (len(local_labels) < 10):
                continue
            business_list =  list(local_labels['business_id']) 
            for i in range(len(business_list)):
                b1 = result.loc[result['business_id'] == business_list[i]]
                for j in range(i+1,len(business_list)):
                    b2 = result.loc[result['business_id'] == business_list[j]]
                    dist = haversine(b1['latitude'].values[0],b1['longitude'].values[0], b2['latitude'].values[0],b2['longitude'].values[0])
                    if(dist <= 2):
                        ratings = abs(b1['stars'].values[0] - b2['stars'].values[0])
                        dist_ratings.append((np.round(dist,4),ratings))
            x = [i for i,j in dist_ratings]
            y = [j for i,j in dist_ratings]
            if(len(x)>0):
                d[cat+"_x"] = x
                d[cat+"_y"] = y
        print(d)
        distances.append(d)
        with open('wyniki/'+filename+'.json', 'a') as f:
            json.dump(d, f)
    with open('wyniki/'+filename+'_all.json', 'w') as f:
        json.dump(distances, f)
    print("stop iteration ",filename)
# 400315

if __name__ == "__main__":

    m1 = multiprocessing.Process(target=user_ratings_distance, args=(0, 200000, "dist_user1"))
    m2 = multiprocessing.Process(target=user_ratings_distance, args=(200001, 400000, "dist_user2"))
    m3 = multiprocessing.Process(target=user_ratings_distance, args=(400001, 600000, "dist_user3"))
    m4 = multiprocessing.Process(target=user_ratings_distance, args=(600001, 800000, "dist_user4"))
    m5 = multiprocessing.Process(target=user_ratings_distance, args=(800001, 1000000, "dist_user5"))
    m6 = multiprocessing.Process(target=user_ratings_distance, args=(1000001, 1200000, "dist_user6"))
    m7 = multiprocessing.Process(target=user_ratings_distance, args=(1200001, 1400000, "dist_user7"))   
    m8 = multiprocessing.Process(target=user_ratings_distance, args=(1400001, 1600000, "dist_user8"))   
    m9 = multiprocessing.Process(target=user_ratings_distance, args=(1600001, len(users_list), "dist_user9"))   
    m1.start()
    m2.start()
    m1.join()
    m2.join()

    m3.start()
    m4.start()
    m3.join()
    m4.join()

    m5.start()
    m6.start()    
    m5.join()
    m6.join()

    m7.start()
    m8.start()
    m7.join()
    m8.join()
    m9.start()
    m9.join()