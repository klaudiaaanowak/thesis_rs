import json
import numpy as np
from collections import Counter
from funcs import load_json_multiple
import operator
import pandas as pd
from sklearn.metrics import mean_squared_error


# # wczytaj business

business = pd.read_csv('without_geo_business_classes.csv',header=0, index_col=0)

classes_labels_list = list(business['class'])
classes_labels = list(dict(Counter(classes_labels_list)))

business_groups = {}
for c in classes_labels:
    business_groups[c] = list((business.loc[business['class'] == c]).index)
with open('without_geo_business_groups_dict.json', 'w') as f:
    json.dump(business_groups,f)

with open('without_geo_business_groups_dict.json', 'r') as f:
    business_groups = json.load(f)


# #wczytaj użytkowników 
users = pd.read_csv('users_classes.csv',header=0, index_col=0)

# # classes_labels_list = list(users['class'])
# # classes_labels = list(dict(Counter(classes_labels_list)))

# # users_groups = {}
# # for c in classes_labels:

# #     users_groups[str(c)] = list((users.loc[users['class'] == c]).index)
# # with open('users_groups_dict.json', 'w') as f:
# #     json.dump(users_groups,f)
with open('users_groups_dict.json', 'r') as f:
    users_groups = json.load(f)


# wczytaj 

review =  pd.read_csv(r'review_dataframe.csv',header=0, index_col=0)
group_review = {}

i = 0
data = {}
for i, row in enumerate(review.values):
    # print(row)
    u = row[0]
    b = row[1]
    star = row[2]
    # print(u, ", ", b, " : ", star)
    try:
        u_class = str(users.loc[u,'class'])
        b_class = business.loc[b,'class']
        # print(u_class, ", ", b_class, " : ", star)
        if (u_class not in data):
            data[u_class] = {}
        if (b_class not in data[u_class]):
            data[u_class][b_class] = {}
            data[u_class][b_class]['star'] = 0
            data[u_class][b_class]['counter'] = 0
        data[u_class][b_class]['star']+=star
        data[u_class][b_class]['counter']+=1

    except:
        print("not found")


ratings = {}
for d in data:
    ratings[d] = {}
    for l in data[d]:
        ratings[d][l] = 0
        if(data[d][l]['counter']>0):
            ratings[d][l] =  round(data[d][l]['star']/data[d][l]['counter'],3)
        else:
            ratings[d][l] = 0
# print(data)
with open('without_geo_temp_ratings_nonzero.json', 'w') as f:
    json.dump(ratings,f)

df = pd.read_json (r'without_geo_temp_ratings_nonzero.json')
df = df.T
df = df.fillna(0)
df.to_csv (r'without_geo_group_review_matrix.csv', index = True, header=True)

with open('without_geo_temp_ratings_nonzero.json', 'r') as f:
    matrix = json.load(f)

review = {}
review['user'] = []
review['business'] =[]
review['star'] = []   
for key in matrix.keys():
    for label in matrix[key].keys():
        review['user'].append(key)
        review['business'].append(label)
        review['star'].append(matrix[key][label])
review_data = pd.DataFrame(review, columns=['user', 'business','star'])
print(review_data.head())
review_data.to_csv (r'without_geo_group_review_dataframe.csv', index = True, header=True)