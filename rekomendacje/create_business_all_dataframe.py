import json
import numpy as np
from collections import Counter
from funcs import load_json_multiple
import operator
import pandas as pd

data = []
with open('data_business.json', 'r', encoding='utf-8',
                 errors='ignore') as f:
   for parsed_json in load_json_multiple(f):
       data.append(parsed_json)

with open('categories.json', 'r', encoding='utf-8',
                 errors='ignore') as f:
                 categories = json.load(f)

classes = pd.read_csv('business_classes.csv',header=0)


list_df = []
for d in data:
    df = {}
    if(d['attributes'] == None):
        d['attributes'] = []
    found_class = classes.loc[classes['business_id'] == d['business_id']]['class'].values
    if (len(found_class) == 0):
        continue
    df['class'] = found_class[0]

    df['business_id'] = d['business_id']
    df['name'] = d['name']
    df['address'] = d['address']
    df['city'] = d['city']
    df['state'] = d['state']
    df['postal_code'] = d['postal_code']
    df['latitude'] = d['latitude']    
    df['longitude'] = d['longitude']    
    df['stars'] = d['stars']
    df['review_count'] = d['review_count']
    df['BusinessAcceptsCreditCards'] = 'BusinessAcceptsCreditCards' in d['attributes']
    df['BikeParking'] = 'BikeParking' in d['attributes']
    df['RestaurantsTakeOut'] = 'RestaurantsTakeOut' in d['attributes']
    df['GoodForKids'] = 'GoodForKids' in d['attributes']
    df['RestaurantsGoodForGroups'] = 'RestaurantsGoodForGroups' in d['attributes']
    df['HasTV'] = 'HasTV' in d['attributes']
    df['ByAppointmentOnly'] = 'ByAppointmentOnly' in d['attributes']
    df['OutdoorSeating'] = 'OutdoorSeating' in d['attributes']
    df['Caters'] = 'Caters' in d['attributes']
    df['RestaurantsReservations'] = 'RestaurantsReservations' in d['attributes']
    df['WheelchairAccessible'] = 'WheelchairAccessible' in d['attributes']
    df['RestaurantsDelivery'] = 'RestaurantsDelivery' in d['attributes']
    df['RestaurantsTableService'] = 'RestaurantsTableService' in d['attributes']
    df['AcceptsInsurance'] = 'AcceptsInsurance' in d['attributes']
    df['HappyHour'] = 'HappyHour' in d['attributes']
    df['DogsAllowed'] = 'DogsAllowed' in d['attributes']
    df['DriveThru'] = 'DriveThru' in d['attributes']
    df['GoodForDancing'] = 'GoodForDancing' in d['attributes']
    df['BusinessAcceptsBitcoin'] = 'BusinessAcceptsBitcoin' in d['attributes']
    df['CoatCheck'] = 'CoatCheck' in d['attributes']
    df['Restaurants'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['Restaurants'])))
    df['Shopping'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['Shopping'])))
    df['Services'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['Services'])))
    df['Beauty'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['Beauty'])))
    df['Healthcare'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['Healthcare'])))
    df['Automotive'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['Automotive'])))
    df['EducationNEntertainment'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['EducationNEntertainment'])))
    df['Home'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['Home'])))
    df['Sport'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['Sport'])))
    df['otherPlacesGastronomy'] = d['otherPlacesGastronomy']
    df['otherPlacesEducation'] = d['otherPlacesEducation']
    df['otherPlacesParking'] = d['otherPlacesParking']
    df['otherPlacesTransport'] = d['otherPlacesTransport']
    df['otherPlacesHealthcare'] = d['otherPlacesHealthcare']
    df['otherPlacesGastronomy'] = d['otherPlacesGastronomy']
    df['otherPlacesEntertainment'] = d['otherPlacesEntertainment']
    df['otherPlacesReligion'] = d['otherPlacesReligion']
    list_df.append(df)


dataframe = pd.DataFrame.from_dict(list_df, orient='columns')
df_to_cluster = dataframe.copy()
del df_to_cluster['business_id']
df_to_cluster = df_to_cluster.set_index(dataframe['business_id'])
df_to_cluster.to_csv (r'business_dataframe_all.csv', index = True, header=True)