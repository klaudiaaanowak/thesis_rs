from sklearn.preprocessing import Normalizer
import pandas as pd
from sklearn.cluster import MiniBatchKMeans
import matplotlib.pyplot as plt
from sklearn import decomposition
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from math import sqrt
scaler = Normalizer()

# data = pd.read_csv('business_dataframe_LasVegas.csv',header=0, index_col=0)
# data = pd.read_csv('business_dataframe_Phoenix.csv',header=0, index_col=0)
data = pd.read_csv('business_dataframe_Toronto.csv',header=0, index_col=0)


data_normalized = data.copy()

data_normalized[['review_count','stars']] = scaler.fit_transform(data_normalized[['review_count','stars']].to_numpy())
data_normalized = data_normalized[['AcceptsInsurance','Automotive','Beauty','BikeParking','BusinessAcceptsBitcoin','BusinessAcceptsCreditCards','ByAppointmentOnly','Caters','CoatCheck','DogsAllowed','DriveThru','EducationNEntertainment','GoodForDancing','GoodForKids','HappyHour','HasTV','Healthcare','Home','OutdoorSeating','Restaurants','RestaurantsDelivery','RestaurantsGoodForGroups','RestaurantsReservations','RestaurantsTableService','RestaurantsTakeOut','Services','Shopping','Sport','WheelchairAccessible','review_count','stars']]
print(data_normalized.shape)

sse_train = []
sse_test = []
sse = []
list_k = list(range(10, 500,10))
split = 10
for k in list_k:
    train_err_sum = 0
    test_err_sum = 0
    for i in range(split):
        # k_means = KMeans(n_clusters=k)
        k_means = MiniBatchKMeans(n_clusters=k, init_size=k)
        X_train, X_test = train_test_split(data_normalized, test_size=0.4)
        k_means.fit(X_train)
        centroids = k_means.cluster_centers_
        centroids_lables = centroids[k_means.labels_]
        err_train = mean_squared_error(X_train.to_numpy(), centroids_lables)
        train_err_sum+=err_train  
        predicted = k_means.predict(X_test)
        centroids_lables_predicted = centroids[predicted]
        test = X_test.to_numpy()
        err_test = mean_squared_error(X_test.to_numpy(), centroids_lables_predicted)
        test_err_sum +=err_test
    train_err_sum = train_err_sum/split
    test_err_sum = test_err_sum/split
    sse_train.append(train_err_sum)
    sse_test.append(test_err_sum/10)
    err_avg = (train_err_sum+test_err_sum)/2
    print("K: ",k, ", err_train: ",train_err_sum, ", err_test: ",test_err_sum,", avg err: ",err_avg)
    sse.append(err_avg)

print("end of loop")
fig, axs = plt.subplots(2)
fig.suptitle('Errors')
axs[0].plot(list_k, sse_train, '-o')
axs[1].plot(list_k, sse_test, '-o')

axs[0].set(xlabel=r'Number of clusters *k*', ylabel='Sum of squared distance')
axs[1].set(xlabel=r'Number of clusters *k*', ylabel='Sum of squared distance')

plt.show()

plt.plot(list_k, sse, '-o')
plt.xlabel(r'Number of clusters *k*')
plt.ylabel('Sum of squared distance')
plt.show()
