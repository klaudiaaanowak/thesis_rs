import json
from  funcs import load_json_multiple
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

with open("categories.json", 'r') as file:
    categories = json.load(file)

categories_list = categories.keys()

businesses = pd.read_csv('business_dataframe.csv',header=0, index_col=0)

# print(len(businesses))
# distribution = {}
# distribution["Restaurants"] = len(businesses[businesses["Restaurants"]])
# distribution["Shopping"] = len(businesses[businesses["Shopping"]])
# distribution["Services"] = len(businesses[businesses["Services"]])
# distribution["Beauty"] = len(businesses[businesses["Beauty"]])
# distribution["Healthcare"] = len(businesses[businesses["Healthcare"]])
# distribution["Automotive"] = len(businesses[businesses["Automotive"]])
# distribution["EducationNEntertainment"] = len(businesses[businesses["EducationNEntertainment"]])
# distribution["Home"] = len(businesses[businesses["Home"]])
# distribution["Sport"] = len(businesses[businesses["Sport"]])
# distribution["NoCategory"] = len(businesses[~businesses["Restaurants"] & ~businesses["Shopping"] & ~businesses["Services"] & ~businesses["Beauty"] & ~businesses["Healthcare"] & ~businesses["EducationNEntertainment"] & ~businesses["Home"]  & ~businesses["Sport"]])

# distribution_sorted = {k: v for k,v in sorted(distribution.items(), key= lambda x : x[1], reverse=True)}
# plt.bar(distribution_sorted.keys(), distribution_sorted.values())
# plt.show()

otherPlaces = {}
otherPlaces["otherPlacesGastronomy"] = np.average(businesses["otherPlacesGastronomy"])
otherPlaces["otherPlacesEducation"] = np.average(businesses["otherPlacesEducation"])
otherPlaces["otherPlacesParking"] = np.average(businesses["otherPlacesParking"])
otherPlaces["otherPlacesTransport"] = np.average(businesses["otherPlacesTransport"])
otherPlaces["otherPlacesHealthcare"] = np.average(businesses["otherPlacesHealthcare"])
otherPlaces["otherPlacesEntertainment"] = np.average(businesses["otherPlacesEntertainment"])
otherPlaces["otherPlacesReligion"] = np.average(businesses["otherPlacesReligion"])

otherPlaces_sorted = {k: v for k,v in sorted(otherPlaces.items(), key= lambda x : x[1], reverse=True)}
plt.bar(otherPlaces_sorted.keys(), otherPlaces_sorted.values())
plt.show()