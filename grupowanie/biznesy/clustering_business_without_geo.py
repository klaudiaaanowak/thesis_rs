from sklearn.preprocessing import Normalizer
import pandas as pd
import json
import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from sklearn import decomposition
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from math import sqrt
scaler = Normalizer()

dataLV = pd.read_csv('business_dataframe_LasVegas.csv',header=0, index_col=0)
# kL = 900
# Error:  0.004041100078883916 - mniejszy niż z geo
kL  = 470

dataPh = pd.read_csv('business_dataframe_Phoenix.csv',header=0, index_col=0)
# kP = 750
#Error: 0.0037121530324533297 - mniejszy niż z geo
kP = 460
dataTo = pd.read_csv('business_dataframe_Toronto.csv',header=0, index_col=0)
# kT = 900
# Error:  0.003915133426494333 - mniejszy niż z geo
kT = 480
dataLV = dataLV[['AcceptsInsurance','Automotive','Beauty','BikeParking','BusinessAcceptsBitcoin','BusinessAcceptsCreditCards','ByAppointmentOnly','Caters','CoatCheck','DogsAllowed','DriveThru','EducationNEntertainment','GoodForDancing','GoodForKids','HappyHour','HasTV','Healthcare','Home','OutdoorSeating','Restaurants','RestaurantsDelivery','RestaurantsGoodForGroups','RestaurantsReservations','RestaurantsTableService','RestaurantsTakeOut','Services','Shopping','Sport','WheelchairAccessible','review_count','stars']]

dataLV_normalized = dataLV.copy()

dataLV_normalized[['review_count','stars']] = scaler.fit_transform(dataLV_normalized[['review_count','stars']].to_numpy())
dataLV_normalized = dataLV_normalized[['AcceptsInsurance','Automotive','Beauty','BikeParking','BusinessAcceptsBitcoin','BusinessAcceptsCreditCards','ByAppointmentOnly','Caters','CoatCheck','DogsAllowed','DriveThru','EducationNEntertainment','GoodForDancing','GoodForKids','HappyHour','HasTV','Healthcare','Home','OutdoorSeating','Restaurants','RestaurantsDelivery','RestaurantsGoodForGroups','RestaurantsReservations','RestaurantsTableService','RestaurantsTakeOut','Services','Shopping','Sport','WheelchairAccessible','review_count','stars']]
dataLV_normalized = np.round(dataLV_normalized,3)
print(dataLV_normalized.shape)
k_means = KMeans(n_clusters=kL)
k_means.fit(dataLV_normalized)
centroidsLV = k_means.cluster_centers_
centroids_lablesLV = centroidsLV[k_means.labels_]
errorLV = mean_squared_error(dataLV_normalized.to_numpy(), centroids_lablesLV)
print("Error: ",errorLV)
print(dataLV_normalized.shape)
print(centroidsLV.shape)

dataLV['class'] = ["LV_" +str(s) for s in k_means.labels_]
dataLV.to_csv (r'new_wihout_geo_business_LV_classes.csv', index = True, header=True)

centroidsLV = np.array(centroidsLV)
centroidsLV[:,:29] = np.round(centroidsLV[:,:29])
centroidsLV[:,:29] = (centroidsLV[:,:29] > 0).tolist()
classesLV = {}

for i in range(centroidsLV.shape[0]):
    classesLV["LV_"+ str(i)] = list(centroidsLV[i])
with open('new_wihout_geo_classes_business_LV.json','w') as out:
    json.dump(classesLV, out)

dataPh = dataPh[['AcceptsInsurance','Automotive','Beauty','BikeParking','BusinessAcceptsBitcoin','BusinessAcceptsCreditCards','ByAppointmentOnly','Caters','CoatCheck','DogsAllowed','DriveThru','EducationNEntertainment','GoodForDancing','GoodForKids','HappyHour','HasTV','Healthcare','Home','OutdoorSeating','Restaurants','RestaurantsDelivery','RestaurantsGoodForGroups','RestaurantsReservations','RestaurantsTableService','RestaurantsTakeOut','Services','Shopping','Sport','WheelchairAccessible','review_count','stars']]

dataPh_normalized = dataPh.copy()

dataPh_normalized[['review_count','stars']] = scaler.fit_transform(dataPh_normalized[['review_count','stars']].to_numpy())
dataPh_normalized = dataPh_normalized[['AcceptsInsurance','Automotive','Beauty','BikeParking','BusinessAcceptsBitcoin','BusinessAcceptsCreditCards','ByAppointmentOnly','Caters','CoatCheck','DogsAllowed','DriveThru','EducationNEntertainment','GoodForDancing','GoodForKids','HappyHour','HasTV','Healthcare','Home','OutdoorSeating','Restaurants','RestaurantsDelivery','RestaurantsGoodForGroups','RestaurantsReservations','RestaurantsTableService','RestaurantsTakeOut','Services','Shopping','Sport','WheelchairAccessible','review_count','stars']]
dataPh_normalized = np.round(dataPh_normalized,3)
print(dataPh_normalized.shape)
k_means = KMeans(n_clusters=kP)
k_means.fit(dataPh_normalized)
centroidsPh = k_means.cluster_centers_
centroids_lablesPh = centroidsPh[k_means.labels_]
errorPh = mean_squared_error(dataPh_normalized.to_numpy(), centroids_lablesPh)
print("Error: ",errorPh)
print(dataPh_normalized.shape)
print(centroidsPh.shape)

dataPh['class'] = ["Ph_" +str(s) for s in k_means.labels_]
dataPh.to_csv (r'new_wihout_geo_business_Ph_classes.csv', index = True, header=True)


centroidsPh = np.array(centroidsPh)
centroidsPh[:,:29] = np.round(centroidsPh[:,:29])
centroidsPh[:,:29] = (centroidsPh[:,:29] > 0).tolist()
classesPh = {}
for i in range(centroidsPh.shape[0]):
    classesPh["Ph_"+ str(i)] = list(centroidsPh[i])
with open('new_wihout_geo_classes_business_Ph.json','w') as out:
    json.dump(classesPh, out)

dataTo = dataTo[['AcceptsInsurance','Automotive','Beauty','BikeParking','BusinessAcceptsBitcoin','BusinessAcceptsCreditCards','ByAppointmentOnly','Caters','CoatCheck','DogsAllowed','DriveThru','EducationNEntertainment','GoodForDancing','GoodForKids','HappyHour','HasTV','Healthcare','Home','OutdoorSeating','Restaurants','RestaurantsDelivery','RestaurantsGoodForGroups','RestaurantsReservations','RestaurantsTableService','RestaurantsTakeOut','Services','Shopping','Sport','WheelchairAccessible','review_count','stars']]

dataTo_normalized = dataTo.copy()

dataTo_normalized[['review_count','stars']] = scaler.fit_transform(dataTo_normalized[['review_count','stars']].to_numpy())
dataTo_normalized = dataTo_normalized[['AcceptsInsurance','Automotive','Beauty','BikeParking','BusinessAcceptsBitcoin','BusinessAcceptsCreditCards','ByAppointmentOnly','Caters','CoatCheck','DogsAllowed','DriveThru','EducationNEntertainment','GoodForDancing','GoodForKids','HappyHour','HasTV','Healthcare','Home','OutdoorSeating','Restaurants','RestaurantsDelivery','RestaurantsGoodForGroups','RestaurantsReservations','RestaurantsTableService','RestaurantsTakeOut','Services','Shopping','Sport','WheelchairAccessible','review_count','stars']]
dataTo_normalized = np.round(dataTo_normalized,3)
print(dataTo_normalized.shape)
k_means = KMeans(n_clusters=kT)
k_means.fit(dataTo_normalized)
centroidsTo = k_means.cluster_centers_
centroids_lablesTo = centroidsTo[k_means.labels_]
errorTo = mean_squared_error(dataTo_normalized.to_numpy(), centroids_lablesTo)
print("Error: ",errorTo)
print(dataTo_normalized.shape)
print(centroidsTo.shape)

print("Full error: ", errorLV + errorPh + errorTo)
exit()

dataTo['class'] = ["To_" +str(s) for s in k_means.labels_]
dataTo.to_csv (r'new_wihout_geo_business_To_classes.csv', index = True, header=True)

centroidsTo = np.array(centroidsTo)
centroidsTo[:,:29] = np.round(centroidsTo[:,:29])
centroidsTo[:,:29] = (centroidsTo[:,:29] > 0).tolist()
classesTo = {}
for i in range(centroidsTo.shape[0]):
    classesTo["To_"+ str(i)] = list(centroidsTo[i])
with open('new_wihout_geo_classes_business_To.json','w') as out:
    json.dump(classesTo, out)


df1 = pd.read_csv('new_wihout_geo_business_LV_classes.csv',header=0, index_col=0)
df2 = pd.read_csv('new_wihout_geo_business_Ph_classes.csv',header=0, index_col=0)
df3 = pd.read_csv('new_wihout_geo_business_To_classes.csv',header=0, index_col=0)


frames = [df1, df2, df3]

result = pd.concat(frames)
result.to_csv (r'new_wihout_geo_business_classes.csv', index = True, header=True)


with open('new_wihout_geo_classes_business_LV.json','r') as out:
    j1= json.load(out)
with open('new_wihout_geo_classes_business_Ph.json','r') as out:
    j2= json.load(out)
with open('new_wihout_geo_classes_business_To.json','r') as out:
    j3= json.load(out)


with open('new_wihout_geo_classes_business_labels.json','a') as out:
    json.dump(j1, out)
with open('new_wihout_geo_classes_business_labels.json','a') as out:
    json.dump(j2, out)
with open('new_wihout_geo_classes_business_labels.json','a') as out:
    json.dump(j3, out)

