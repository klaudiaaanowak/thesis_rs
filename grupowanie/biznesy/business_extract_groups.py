import pandas as pd
import operator
from collections import Counter
import matplotlib.pyplot as plt
import numpy as np


data = pd.read_csv('new_business_classes.csv',header=0, index_col=0)

classes_labels_list = list(data['class'])
classes_labels = dict(Counter(classes_labels_list))
classes_labels = dict(sorted(classes_labels.items(), key=operator.itemgetter(1),reverse=True))
classes_labels_max = dict((k, v) for k, v in classes_labels.items() if v > 100)

data_max = data[data['class'].isin(list(classes_labels_max.keys()))] 
data_max.to_csv (r'new_business_dataframe_100max.csv', index = True, header=True)


classes_labels_list = list(data['class'])
classes_labels = dict(Counter(classes_labels_list))
classes_labels = dict(sorted(classes_labels.items(), key=operator.itemgetter(1),reverse=True))
classes_labels_max = dict((k, v) for k, v in classes_labels.items() if v > 50)

data_max = data[data['class'].isin(list(classes_labels_max.keys()))] 
data_max.to_csv (r'new_business_dataframe_50max.csv', index = True, header=True)

classes_labels_list = list(data['class'])
classes_labels = dict(Counter(classes_labels_list))
classes_labels = dict(sorted(classes_labels.items(), key=operator.itemgetter(1),reverse=True))
classes_labels_max = {k: classes_labels[k] for k in list(classes_labels)[:10]}

data_max = data[data['class'].isin(list(classes_labels_max.keys()))] 
data_max.to_csv (r'new_business_dataframe_top10.csv', index = True, header=True)



data = pd.read_csv('new_wihout_geo_business_classes.csv',header=0, index_col=0)
classes_labels_list = list(data['class'])
classes_labels = dict(Counter(classes_labels_list))
classes_labels = dict(sorted(classes_labels.items(), key=operator.itemgetter(1),reverse=True))
classes_labels_max = dict((k, v) for k, v in classes_labels.items() if v > 100)

data_max = data[data['class'].isin(list(classes_labels_max.keys()))] 
data_max.to_csv (r'new_wihout_geo_business_dataframe_100max.csv', index = True, header=True)


classes_labels_list = list(data['class'])
classes_labels = dict(Counter(classes_labels_list))
classes_labels = dict(sorted(classes_labels.items(), key=operator.itemgetter(1),reverse=True))
classes_labels_max = dict((k, v) for k, v in classes_labels.items() if v > 50)

data_max = data[data['class'].isin(list(classes_labels_max.keys()))] 
data_max.to_csv (r'new_wihout_geo_business_dataframe_50max.csv', index = True, header=True)

classes_labels_list = list(data['class'])
classes_labels = dict(Counter(classes_labels_list))
classes_labels = dict(sorted(classes_labels.items(), key=operator.itemgetter(1),reverse=True))
classes_labels_max = {k: classes_labels[k] for k in list(classes_labels)[:10]}

data_max = data[data['class'].isin(list(classes_labels_max.keys()))] 
data_max.to_csv (r'new_wihout_geo_business_dataframe_top10.csv', index = True, header=True)
