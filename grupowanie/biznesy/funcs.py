import json
import requests
import numpy as np 

def load_json_multiple(segments):
    chunk = ""
    for segment in segments:
        chunk += segment
        try:
            yield json.loads(chunk)
            chunk = ""
        except ValueError:
            pass


def move_coordinates(lat, lon, dist, degree):
    # dist in meters
    R = 6367000
    dist = dist/R
    degree = np.radians(degree)
    lat =  np.radians(lat)
    lon = np.radians(lon)
    latB = np.arcsin(np.sin(lat)*(np.cos(dist))+np.cos(lat)*np.sin(dist)*np.cos(degree))
    lonB = lon + np.arctan2(np.sin(degree)*np.sin(dist)*np.cos(lat),np.cos(dist)-np.sin(lat)*np.sin(latB))
    return np.degrees(latB), np.degrees(lonB)

def get_near_area_boundaries(lat, lon, dist):
    degreeTopRight = 45
    degreeBottomLeft = -135
    topRight = move_coordinates(lat, lon, dist, degreeTopRight)
    bottomLeft = move_coordinates(lat, lon, dist, degreeBottomLeft)
    return topRight[1], topRight[0], bottomLeft[0], bottomLeft[1]


def get_objects_from_neighborhood(lat,lon,dist,object_type):
    boundaries = get_near_area_boundaries(lat, lon, dist)
    overpass_url = "http://overpass-api.de/api/interpreter"
    overpass_query = """<osm-script output="json">
    <union>
        <query type="node">
        <has-kv k="amenity" regv="{type}"/>
        <bbox-query e="{0}" n="{1}" s="{2}" w="{3}"/>
        </query>
    </union>
    <print mode="body"/>
    <recurse type="down"/>
    <print mode="skeleton"/>
    </osm-script>
    """.format(boundaries[0], boundaries[1], boundaries[2], boundaries[3],type = object_type)
    response = requests.get(overpass_url, 
                        params={'data': overpass_query})
    responseJson = ""
    try:
        responseJson = response.json()
    except:
        print("none")
        response = requests.get(overpass_url, 
                        params={'data': overpass_query})
        try:
            responseJson = response.json()
        except:
            print("no_val")

               
    return responseJson
