from sklearn.preprocessing import Normalizer
import pandas as pd
import json
import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from sklearn import decomposition
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from math import sqrt
scaler = Normalizer()

data_LV = pd.read_csv('business_dataframe_LasVegas.csv',header=0, index_col=0)
#kL = 850
## Error:  0.00616327939888475
kL = 480

data_Ph = pd.read_csv('business_dataframe_Phoenix.csv',header=0, index_col=0)
#kP = 750
##Error: 0.006312630812118014
kP = 500

data_To = pd.read_csv('business_dataframe_Toronto.csv',header=0, index_col=0)
#kT = 850
## Error:  0.004589171155863396
kT = 460

data_LV_normalized = data_LV.copy()

data_LV_normalized[['otherPlacesEducation','otherPlacesEntertainment','otherPlacesGastronomy','otherPlacesHealthcare','otherPlacesParking','otherPlacesReligion','otherPlacesTransport','review_count','stars']] = scaler.fit_transform(data_LV_normalized[['otherPlacesEducation','otherPlacesEntertainment','otherPlacesGastronomy','otherPlacesHealthcare','otherPlacesParking','otherPlacesReligion','otherPlacesTransport','review_count','stars']].to_numpy())
data_LV_normalized = np.round(data_LV_normalized,3)

k_means = KMeans(n_clusters=kL)
k_means.fit(data_LV_normalized)
centroidsLV = k_means.cluster_centers_
centroids_lablesLV = centroidsLV[k_means.labels_]
errorLV = mean_squared_error(data_LV_normalized.to_numpy(), centroids_lablesLV)
print("Error: ",errorLV)


data_LV['class'] = ["LV_" +str(s) for s in k_means.labels_]
data_LV.to_csv (r'new_business_LV_classes.csv', index = True, header=True)

centroidsLV = np.array(centroidsLV)
centroidsLV[:,:29] = np.round(centroidsLV[:,:29])
centroidsLV[:,:29] = (centroidsLV[:,:29] > 0).tolist()
classesLV = {}

for i in range(centroidsLV.shape[0]):
    classesLV["LV_"+ str(i)] = list(centroidsLV[i])
with open('new_classes_business_LV.json','w') as out:
    json.dump(classesLV, out)


data_Ph_normalized = data_Ph.copy()

data_Ph_normalized[['otherPlacesEducation','otherPlacesEntertainment','otherPlacesGastronomy','otherPlacesHealthcare','otherPlacesParking','otherPlacesReligion','otherPlacesTransport','review_count','stars']] = scaler.fit_transform(data_Ph_normalized[['otherPlacesEducation','otherPlacesEntertainment','otherPlacesGastronomy','otherPlacesHealthcare','otherPlacesParking','otherPlacesReligion','otherPlacesTransport','review_count','stars']].to_numpy())
data_Ph_normalized = np.round(data_Ph_normalized,3)

k_means = KMeans(n_clusters=kL)
k_means.fit(data_Ph_normalized)
centroidsPh = k_means.cluster_centers_
centroids_lablesPh = centroidsPh[k_means.labels_]
errorPh = mean_squared_error(data_Ph_normalized.to_numpy(), centroids_lablesPh)
print("Error: ",errorPh)

data_Ph['class'] = ["Ph_" +str(s) for s in k_means.labels_]
data_Ph.to_csv (r'new_business_Ph_classes.csv', index = True, header=True)


centroidsPh = np.array(centroidsPh)
centroidsPh[:,:29] = np.round(centroidsPh[:,:29])
centroidsPh[:,:29] = (centroidsPh[:,:29] > 0).tolist()
classesPh = {}
for i in range(centroidsPh.shape[0]):
    classesPh["Ph_"+ str(i)] = list(centroidsPh[i])
with open('new_classes_business_Ph.json','w') as out:
    json.dump(classesPh, out)


data_To_normalized = data_To.copy()

data_To_normalized[['otherPlacesEducation','otherPlacesEntertainment','otherPlacesGastronomy','otherPlacesHealthcare','otherPlacesParking','otherPlacesReligion','otherPlacesTransport','review_count','stars']] = scaler.fit_transform(data_To_normalized[['otherPlacesEducation','otherPlacesEntertainment','otherPlacesGastronomy','otherPlacesHealthcare','otherPlacesParking','otherPlacesReligion','otherPlacesTransport','review_count','stars']].to_numpy())
data_To_normalized = np.round(data_To_normalized,3)

k_means = KMeans(n_clusters=kL)
k_means.fit(data_To_normalized)
centroidsTo = k_means.cluster_centers_
centroids_lablesTo = centroidsTo[k_means.labels_]
errorTo = mean_squared_error(data_To_normalized.to_numpy(), centroids_lablesTo)
print("Error: ",errorTo)
data_To['class'] = ["To_" +str(s) for s in k_means.labels_]
data_To.to_csv (r'new_business_To_classes.csv', index = True, header=True)

print("Full error: ", errorLV + errorPh + errorTo)

centroidsTo = np.array(centroidsTo)
centroidsTo[:,:29] = np.round(centroidsTo[:,:29])
centroidsTo[:,:29] = (centroidsTo[:,:29] > 0).tolist()
classesTo = {}
for i in range(centroidsTo.shape[0]):
    classesTo["To_"+ str(i)] = list(centroidsTo[i])
with open('new_classes_business_To.json','w') as out:
    json.dump(classesTo, out)




df1 = pd.read_csv('new_business_LV_classes.csv',header=0, index_col=0)
df2 = pd.read_csv('new_business_Ph_classes.csv',header=0, index_col=0)
df3 = pd.read_csv('new_business_To_classes.csv',header=0, index_col=0)


frames = [df1, df2, df3]

result = pd.concat(frames)
result.to_csv (r'new_business_classes.csv', index = True, header=True)

with open('new_classes_business_LV.json','r') as out:
    j1= json.load(out)
with open('new_classes_business_Ph.json','r') as out:
    j2= json.load(out)
with open('new_classes_business_To.json','r') as out:
    j3= json.load(out)


with open('new_classes_business_labels.json','a') as out:
    json.dump(j1, out)
with open('new_classes_business_labels.json','a') as out:
    json.dump(j2, out)
with open('new_classes_business_labels.json','a') as out:
    json.dump(j3, out)

