import json
import numpy as np
from collections import Counter
from funcs import load_json_multiple
import operator
import pandas as pd
from sklearn.cluster import MiniBatchKMeans
import matplotlib.pyplot as plt
from sklearn import decomposition
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from math import sqrt

data = []
with open('data_business.json', 'r', encoding='utf-8',
                 errors='ignore') as f:
   for parsed_json in load_json_multiple(f):
       data.append(parsed_json)

cityList = (list(map(operator.itemgetter('city'), data)))
categoriesList = (list(map(operator.itemgetter('categories'), data)))

cities = dict(Counter(cityList))
mostCommonCities = {k:v for k, v in cities.items() if v>10}
mostCommonCities = dict(sorted(cities.items(), key=operator.itemgetter(1), reverse=True))

with open("cities.json", "w") as write_file:
    json.dump(mostCommonCities, write_file)


with open('categories.json', 'r', encoding='utf-8',
                 errors='ignore') as f:
                 categories = json.load(f)

list_df = []
for d in data:
    df = {}
    if(d['attributes'] == None):
        d['attributes'] = []
    df['business_id'] = d['business_id']
    df['city'] = d['city']
    df['stars'] = d['stars']
    df['review_count'] = d['review_count']
    df['BusinessAcceptsCreditCards'] = 'BusinessAcceptsCreditCards' in d['attributes']
    df['BikeParking'] = 'BikeParking' in d['attributes']
    df['RestaurantsTakeOut'] = 'RestaurantsTakeOut' in d['attributes']
    df['GoodForKids'] = 'GoodForKids' in d['attributes']
    df['RestaurantsGoodForGroups'] = 'RestaurantsGoodForGroups' in d['attributes']
    df['HasTV'] = 'HasTV' in d['attributes']
    df['ByAppointmentOnly'] = 'ByAppointmentOnly' in d['attributes']
    df['OutdoorSeating'] = 'OutdoorSeating' in d['attributes']
    df['Caters'] = 'Caters' in d['attributes']
    df['RestaurantsReservations'] = 'RestaurantsReservations' in d['attributes']
    df['WheelchairAccessible'] = 'WheelchairAccessible' in d['attributes']
    df['RestaurantsDelivery'] = 'RestaurantsDelivery' in d['attributes']
    df['RestaurantsTableService'] = 'RestaurantsTableService' in d['attributes']
    df['AcceptsInsurance'] = 'AcceptsInsurance' in d['attributes']
    df['HappyHour'] = 'HappyHour' in d['attributes']
    df['DogsAllowed'] = 'DogsAllowed' in d['attributes']
    df['DriveThru'] = 'DriveThru' in d['attributes']
    df['GoodForDancing'] = 'GoodForDancing' in d['attributes']
    df['BusinessAcceptsBitcoin'] = 'BusinessAcceptsBitcoin' in d['attributes']
    df['CoatCheck'] = 'CoatCheck' in d['attributes']
    df['Restaurants'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['Restaurants'])))
    df['Shopping'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['Shopping'])))
    df['Services'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['Services'])))
    df['Beauty'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['Beauty'])))
    df['Healthcare'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['Healthcare'])))
    df['Automotive'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['Automotive'])))
    df['EducationNEntertainment'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['EducationNEntertainment'])))
    df['Home'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['Home'])))
    df['Sport'] = np.any(np.in1d(np.array(d['categories']),np.array(categories['Sport'])))
    df['otherPlacesGastronomy'] = d['otherPlacesGastronomy']
    df['otherPlacesEducation'] = d['otherPlacesEducation']
    df['otherPlacesParking'] = d['otherPlacesParking']
    df['otherPlacesTransport'] = d['otherPlacesTransport']
    df['otherPlacesHealthcare'] = d['otherPlacesHealthcare']
    df['otherPlacesGastronomy'] = d['otherPlacesGastronomy']
    df['otherPlacesEntertainment'] = d['otherPlacesEntertainment']
    df['otherPlacesReligion'] = d['otherPlacesReligion']
    list_df.append(df)


dataframe = pd.DataFrame.from_dict(list_df, orient='columns')
df_to_cluster = dataframe.copy()
del df_to_cluster['business_id']
df_to_cluster = df_to_cluster.set_index(dataframe['business_id'])
df_to_cluster.to_csv (r'business_dataframe.csv', index = True, header=True)

data = pd.read_csv('business_dataframe.csv',header=0, index_col=0)



data_LasVegas = data.loc[data['city'] == 'Las Vegas'] 
data_Phoenix = data.loc[data['city'] == 'Phoenix'] 
data_Toronto = data.loc[data['city'] == 'Toronto'] 

dataframe_LasVegas = pd.DataFrame.from_dict(data_LasVegas, orient='columns')
del dataframe_LasVegas['city']
dataframe_LasVegas.to_csv (r'business_dataframe_LasVegas.csv', index = True, header=True)

dataframe_Phoenix = pd.DataFrame.from_dict(data_Phoenix, orient='columns')
del dataframe_Phoenix['city']
dataframe_Phoenix.to_csv (r'business_dataframe_Phoenix.csv', index = True, header=True)

dataframe_Toronto = pd.DataFrame.from_dict(data_Toronto, orient='columns')
del dataframe_Toronto['city']
dataframe_Toronto.to_csv (r'business_dataframe_Toronto.csv', index = True, header=True)

