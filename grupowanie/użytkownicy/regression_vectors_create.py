import json
import numpy as np
from funcs import load_json_multiple
import pandas as pd
from collections import Counter

with open("users_regression_all.json",'r') as f:
    users_regr = json.load(f)
with open("categories.json",'r') as f:
    categories_file = json.load(f)

categories = list(categories_file.keys())

# data = []
# with open('data_users.json', 'r', encoding='utf-8',
#                  errors='ignore') as f:
#     i = 0
#     for parsed_json in load_json_multiple(f):
#         # if(i<400400):
#         user = parsed_json['user_id']
#         for cat in categories_file:
#             try:
#                 parsed_json[cat] = users_regr[user][cat+"_x"]
#             except:
#                 parsed_json[cat] = 0
#         data.append(parsed_json)
#         with open("user_data_regr_2_all.json",'a') as f:
#             json.dump(parsed_json, f)
#             f.write('\n')
#         i+=1
#     # else:
#     #     break
# # print(data)
# with open("user_data_regr_all.json",'w') as f:
#     json.dump(data, f)




with open("user_data_regr_all.json",'r') as f:
    reg_data = json.load(f)

# dict_cat =  {}
# for cat in categories:
#     dict_cat[cat] = []

# for d in reg_data:
#     for cat in categories:
#         dict_cat[cat].append(np.round(d[cat],3))

df = pd.DataFrame.from_dict(reg_data, orient='columns')
df_to_cluster = pd.DataFrame(df,columns=['months_on_yelp','review_per_month','useful_per_month','funny_per_month','cool_per_month','reaction/comments','friend_count','fans','average_stars','activity_level',"Restaurants", "Shopping", "Services", "Beauty", "Healthcare", "Automotive", "EducationNEntertainment", "Home", "Sport"])
df_to_cluster = df_to_cluster.set_index(df['user_id'])
df_to_cluster.to_csv (r'users_regression_all_dataframe.csv', index = True, header=True)
