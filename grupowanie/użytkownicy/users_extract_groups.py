import pandas as pd
import operator
from collections import Counter
import matplotlib.pyplot as plt
import numpy as np

data = pd.read_csv('users_classes.csv',header=0, index_col=0)

classes_labels_list = list(data['class'])
classes_labels = dict(Counter(classes_labels_list))
classes_labels = dict(sorted(classes_labels.items(), key=operator.itemgetter(1),reverse=True))
classes_labels_max = dict((k, v) for k, v in classes_labels.items() if v > 1000)

data_max = data[data['class'].isin(list(classes_labels_max.keys()))] 


data_max.to_csv (r'users_dataframe_1000max.csv', index = True, header=True)

classes_labels_list = list(data['class'])
classes_labels = dict(Counter(classes_labels_list))
classes_labels = dict(sorted(classes_labels.items(), key=operator.itemgetter(1),reverse=True))
classes_labels_max = dict((k, v) for k, v in classes_labels.items() if v > 2000)

data_max = data[data['class'].isin(list(classes_labels_max.keys()))] 


data_max.to_csv (r'users_dataframe_2000max.csv', index = True, header=True)

classes_labels_list = list(data['class'])
classes_labels = dict(Counter(classes_labels_list))
classes_labels = dict(sorted(classes_labels.items(), key=operator.itemgetter(1),reverse=True))
classes_labels_max = dict((k, v) for k, v in classes_labels.items() if v > 3000)

data_max = data[data['class'].isin(list(classes_labels_max.keys()))] 


data_max.to_csv (r'users_dataframe_3000max.csv', index = True, header=True)


classes_labels_list = list(data['class'])
classes_labels = dict(Counter(classes_labels_list))
classes_labels = dict(sorted(classes_labels.items(), key=operator.itemgetter(1),reverse=True))
classes_labels_max = {k: classes_labels[k] for k in list(classes_labels)[:10]}

data_max = data[data['class'].isin(list(classes_labels_max.keys()))] 


data_max.to_csv (r'users_dataframe_top10.csv', index = True, header=True)



data_regr = pd.read_csv('users_regression_classes_all.csv',header=0, index_col=0)
classes_labels_list = list(data_regr['class'])
classes_labels = dict(Counter(classes_labels_list))
classes_labels = dict(sorted(classes_labels.items(), key=operator.itemgetter(1),reverse=True))
classes_labels_max = dict((k, v) for k, v in classes_labels.items() if v > 1000)

data_max = data[data['class'].isin(list(classes_labels_max.keys()))] 

data_max.to_csv (r'users_regression_dataframe_1000max.csv', index = True, header=True)


classes_labels_list = list(data_regr['class'])
classes_labels = dict(Counter(classes_labels_list))
classes_labels = dict(sorted(classes_labels.items(), key=operator.itemgetter(1),reverse=True))
classes_labels_max = {k: classes_labels[k] for k in list(classes_labels)[:10]}

data_max = data[data['class'].isin(list(classes_labels_max.keys()))] 


data_max.to_csv (r'users_regression_dataframe_top10.csv', index = True, header=True)
