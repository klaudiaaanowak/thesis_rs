from sklearn.preprocessing import StandardScaler
import pandas as pd
import json
import numpy as np
from sklearn.cluster import MiniBatchKMeans
import matplotlib.pyplot as plt
from sklearn import decomposition
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from math import sqrt
scaler = StandardScaler()

data = pd.read_csv('users_dataframe.csv',header=0, index_col=0)
k = 1000
# # Error:  0.33800353590411497

data_normalized = data.copy()

data_normalized[['months_on_yelp','review_per_month','useful_per_month','funny_per_month','cool_per_month','reaction/comments','friend_count','fans','average_stars','activity_level']] = scaler.fit_transform(data_normalized[['months_on_yelp','review_per_month','useful_per_month','funny_per_month','cool_per_month','reaction/comments','friend_count','fans','average_stars','activity_level']].to_numpy())

k_means = MiniBatchKMeans(n_clusters=k, init_size=k)
k_means.fit(data_normalized)
centroids = k_means.cluster_centers_
centroids_lables = centroids[k_means.labels_]
error = mean_squared_error(data_normalized.to_numpy(), centroids_lables)
print("Error: ",error)

data['class'] = k_means.labels_
data.to_csv (r'users_classes.csv', index = True, header=True)


classes = {}
for i in range(centroids.shape[0]):
    classes[str(i)] = list(centroids[i])


with open('classes_users.json','w') as out:
    json.dump(classes, out)