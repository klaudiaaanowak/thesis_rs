import json
import numpy as np
from funcs import load_json_multiple
import pandas as pd
from sklearn.cluster import MiniBatchKMeans
import matplotlib.pyplot as plt
from sklearn import decomposition
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from math import sqrt

data = []
with open('data_users.json', 'r', encoding='utf-8',
                 errors='ignore') as f:
   for parsed_json in load_json_multiple(f):
       data.append(parsed_json)

df = pd.DataFrame.from_dict(data, orient='columns')
df_to_cluster = pd.DataFrame(df,columns=['months_on_yelp','review_per_month','useful_per_month','funny_per_month','cool_per_month','reaction/comments','friend_count','fans','average_stars','activity_level'])
df_to_cluster = df_to_cluster.set_index(df['user_id'])
df_to_cluster.to_csv (r'users_dataframe.csv', index = True, header=True)


df_to_cluster = pd.read_csv('users_dataframe.csv',header=0, index_col=0)
