import json
import numpy as np
from funcs import load_json_multiple
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import linear_model

data = []
with open('users_distances.json', 'r', encoding='utf-8',
                 errors='ignore') as f:
                 data = json.load(f)
print(len(data))
print(data[0])
# 400315
regr = linear_model.LinearRegression()

users_regr = {}
for u in data:
    keys = list(u.keys())
    user_name = u[keys[0]] 
    users_regr[user_name] = {}
    categories = keys[1:]
    for i in range(0,len(categories),2):
        x = np.array(u[categories[i]]).reshape(-1,1)
        y = np.array(u[categories[i+1]]).reshape(-1,1)
        regr.fit(x, y)
        # print(regr.coef_)
        users_regr[user_name][categories[i]] = regr.coef_[0][0]
# print(users_regr)
with open("users_regression_all.json",'w') as f:
    json.dump(users_regr, f)