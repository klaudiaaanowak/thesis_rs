from enum import Enum
class Gastronomy(Enum):
    BAR = 'bar'
    CAFE = 'cafe'
    FASTFOOD = 'fast-food'
    ICECREAM = 'ice-cream'
    PUB = 'pub'
    RESTAURANT = 'restaurant'

class Education(Enum):
    COLLAGE = 'college'
    KINDERGARTEN = 'kindergatnen'
    LIBRARY = 'library'
    SCHOOL = 'school'
    UNIVERSITY = 'university'

class Parking(Enum):
    PARKING = 'parking'
    PARKING_SPACE = 'paking_space'
    PARKING_BICYCLE = 'bicycle_parking'
    PARKING_MOROCYCLE = 'motorcycle_parking'

class Transport(Enum):
    TAXI = 'taxi'
    BUS_STATION = 'bus_station'
    CAR_RENTAL = 'car_rental'
    BICYCLE_RENTAL = 'bicycle_rental'

class Healthcare(Enum):
    HOSPITAL = 'hospital'
    DOCTORS = 'doctors'
    NURSES = 'nursing_home'
    PHARMACY = 'pharmacy'
class Entertainment(Enum):
    ART_CENTRE = 'art_centre'
    CINEMA = 'cinema'
    THEATRE = 'theatre'

class Other(Enum):
    PLACE_OF_WORSHIP = 'place_of_worship'
