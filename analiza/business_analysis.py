import json
import numpy as np
from collections import Counter
from funcs import load_json_multiple
import operator

data = []
##### You can download file from: https://www.yelp.com/dataset
with open('business.json', 'r', encoding='utf-8',
                 errors='ignore') as f:
   for parsed_json in load_json_multiple(f):
       data.append(parsed_json)

categoriesStringList =(list(map(operator.itemgetter('categories'), data)))

categoriesList = []
for category in categoriesStringList:
    if(category):
        categorySplitted = category.split(",")
        categorySplitted = [x.strip() for x in categorySplitted]
        categoriesList.extend(categorySplitted)
catogoiesCount = dict(Counter(categoriesList))
sorted_categories = dict(sorted(catogoiesCount.items(), key=operator.itemgetter(1), reverse = True))
with open("categories.json", "w") as write_file:
    json.dump(sorted_categories, write_file)
attributesJsonList = (list(map(operator.itemgetter('attributes'), data)))

attributesList =[]
for attributes in attributesJsonList:
    if(attributes):
        attributes = dict(attributes)        
        keysList = attributes.keys()
        for key in keysList:
            if attributes[key] == "True":
                attributesList.append(key)

attributesCount = dict(Counter(attributesList))
sorted_atributes = dict(sorted(attributesCount.items(), key=operator.itemgetter(1), reverse = True))
with open("attributes.json", "w") as write_file:
    json.dump(sorted_atributes, write_file)

newdata = []
for d in data:
    if(d['is_open']==1):
        d.pop('hours', None)
        attributes = d['attributes']
        if(attributes):
            attributesList = []
            attributes = dict(attributes)        
            keysList = attributes.keys()
            for key in keysList:
                if attributes[key] == "True":
                    attributesList.append(key)
            d['attributes'] = attributesList
            category = d['categories']
        if(category):
            categorySplitted = category.split(",")
            categorySplitted = [x.strip() for x in categorySplitted]
            d['categories'] = categorySplitted 
        newdata.append(d)

with open("business_opened_atributes.json", "w") as write_file:
    json.dump(newdata, write_file)