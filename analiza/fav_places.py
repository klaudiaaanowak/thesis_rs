import json
import numpy as np
from funcs import *
from Places import *

alltypes = []
gastronomy = []
for x in Gastronomy:
    gastronomy.append(x.value)
alltypes.extend(gastronomy)
education = []
for x in Education:
    education.append(x.value)
alltypes.extend(education)

parking = []
for x in Parking:
    parking.append(x.value)
alltypes.extend(parking)

transport = []
for x in Transport:
    transport.append(x.value)
alltypes.extend(transport)

healthcare = []
for x in Healthcare:
    healthcare.append(x.value)
alltypes.extend(healthcare)

entertainment = []
for x in Entertainment:
    entertainment.append(x.value)
alltypes.extend(entertainment)
alltypes.extend(Other.PLACE_OF_WORSHIP.value)
objectType = '|'.join(alltypes)

with open('business_opened_atributes.json', 'r', encoding='utf-8',
                 errors='ignore') as f:
                 data = json.load(f)
import time
start = time.time()
for i in range(1,len(data)):
    b = data[i]
    count_gastronomy=0
    count_education=0
    count_parking=0
    count_transport=0
    count_healthcare=0
    count_entertainment=0
    count_religion = 0
    response = get_objects_from_neighborhood(b['latitude'],b['longitude'],2000,objectType)
    if(response!=""):
        for d in response['elements']:
            if d['tags']['amenity'] in gastronomy:
                count_gastronomy+=1
            if d['tags']['amenity'] in education:
                count_education+=1
            if d['tags']['amenity'] in parking:
                count_parking+=1
            if d['tags']['amenity'] in transport:
                count_transport+=1
            if d['tags']['amenity'] in healthcare:
                count_healthcare+=1
            if d['tags']['amenity'] in entertainment:
                count_entertainment+=1
            if d['tags']['amenity'] == Other.PLACE_OF_WORSHIP.value:
                count_religion+=1

    b['otherPlacesGastronomy'] = count_gastronomy
    b['otherPlacesEducation'] = count_education
    b['otherPlacesParking'] = count_parking
    b['otherPlacesTransport'] = count_transport
    b['otherPlacesHealthcare'] = count_healthcare
    b['otherPlacesEntertainment'] = count_entertainment
    b['otherPlacesReligion'] = count_religion
    
    # with open("business_interesting_places_test_9.json", "a") as write_file:
    #         write_file.write(json.dumps(data[0])) 
    #         write_file.write("\n")
    print(i)
end = time.time()
print(end - start) 
with open("data_business.json", "w") as write_file:
    json.dump(data, write_file)
